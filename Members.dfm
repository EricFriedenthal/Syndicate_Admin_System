object Form2: TForm2
  Left = 0
  Top = 110
  Width = 418
  Height = 475
  Caption = 'frmMembers'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object DBGrid1: TDBGrid
    Left = 0
    Top = 0
    Width = 410
    Height = 377
    Align = alTop
    DataSource = DataModule1.dsMember
    TabOrder = 0
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    Columns = <
      item
        Expanded = False
        FieldName = 'ID'
        Width = 25
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'EntityName'
        Width = 102
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'ContactPerson'
        Width = 123
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Tel'
        Visible = True
      end>
  end
  object DBNavigator1: TDBNavigator
    Left = 1
    Top = 384
    Width = 400
    Height = 25
    DataSource = DataModule1.dsMember
    TabOrder = 1
  end
  object Button1: TButton
    Left = 320
    Top = 416
    Width = 75
    Height = 25
    Caption = 'Print List'
    TabOrder = 2
    OnClick = Button1Click
  end
end
