unit Periods;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ComCtrls, Grids, DBGrids;

type
  TForm3 = class(TForm)
    MonthCalendar1: TMonthCalendar;
    Label3: TLabel;
    DBGrid1: TDBGrid;
    Button1: TButton;
    Label1: TLabel;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure Button1Click(Sender: TObject);
  private
    { Private declarations }
  public
   No : Integer;
    { Public declarations }
  end;

var
  Form3: TForm3;

implementation

uses Main, data;

{$R *.DFM}

procedure TForm3.FormClose(Sender: TObject; var Action: TCloseAction);
var i1 : Integer;
begin
 if data.DataModule1.tPeriod.Modified then
  data.DataModule1.tPeriod.Post;
end;

procedure TForm3.Button1Click(Sender: TObject);
var vDate : TDateTime;
begin
  data.DataModule1.QUPD2.SQL.Text := 'update week set startdate = :pstartdate where weekno = :pweekno';
  datamodule1.tweek.First;
  vDate := MonthCalendar1.Date;
  while not datamodule1.tweek.Eof do
  begin
    data.DataModule1.QUPD2.Parameters.ParamByName('pstartdate').value := vDate;
    data.DataModule1.QUPD2.Parameters.ParamByName('pweekno').value := datamodule1.tweek['weekno'];
    data.DataModule1.QUPD2.ExecSQL;
//    showmessage('busy');
    vDate := vDate + 7;
    datamodule1.tweek.Next;
  end;
  datamodule1.tweek.Requery;
  Showmessage('Weeks table updated sucessfully');
end;

end.
