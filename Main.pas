unit Main;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, Registry,
  Menus, StdCtrls, ExtCtrls, ComCtrls, DBCtrls;

type
  TForm1 = class(TForm)
    Panel1: TPanel;
    Label1: TLabel;
    MainMenu1: TMainMenu;
    File1: TMenuItem;
    Libraries1: TMenuItem;
    Members1: TMenuItem;
    Periods1: TMenuItem;
    Exit1: TMenuItem;
    Exit2: TMenuItem;
    About1: TMenuItem;
    GroupBox1: TGroupBox;
    StatusBar1: TStatusBar;
    Label2: TLabel;
    Preferences1: TMenuItem;
    DBLookupComboBox1: TDBLookupComboBox;
    Button1: TButton;
    PreferencesTemplate1: TMenuItem;
    Button2: TButton;
    Button3: TButton;
    Button4: TButton;
    Button5: TButton;
    sd: TSaveDialog;
    Label3: TLabel;
    Panel2: TPanel;
    Image1: TImage;
    Button6: TButton;
    od: TOpenDialog;
    Button7: TButton;
    od2: TOpenDialog;
    procedure FormCreate(Sender: TObject);
    procedure Members1Click(Sender: TObject);
    procedure Periods1Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure Exit1Click(Sender: TObject);
    procedure GroupBox1Click(Sender: TObject);
    procedure Preferences1Click(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure PreferencesTemplate1Click(Sender: TObject);
    procedure About1Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure Button4Click(Sender: TObject);
    procedure SetWeeks;
    procedure Button5Click(Sender: TObject);
    procedure Button6Click(Sender: TObject);
    procedure Button7Click(Sender: TObject);

  private
    { Private declarations }
  public
     Registry: TRegistry;
     S: string;
     i1, cnt1, i, cnt : integer;
     Member, Period : array of String;
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

uses Members, Periods, Preferences, data, Report, About, PrefSum, AllocSum,
  WeekMember, PrefTot, PrefSum1;

{$R *.DFM}

procedure TForm1.FormCreate(Sender: TObject);

begin
{  Registry := TRegistry.Create;
  Registry.RootKey := HKEY_LOCAL_MACHINE;
  Registry.OpenKey('\Software\Syndicate\Members',true);
  cnt := 0;
  While  length(Registry.ReadString('Member'+inttostr(cnt))) > 0  do   inc(cnt);
  SetLength(Member, cnt);
  i := 0;
  While  length(Registry.ReadString('Member'+inttostr(i))) > 0  do
  begin
   Member[i] := Registry.ReadString('Member'+inttostr(i));
   inc(i);
  end;


  //   Registry.WriteString('Member'+inttostr(10),'Saxton, Mr B');

  Registry.OpenKey('\Software\Syndicate\Periods',true);
  cnt1 := 0;
  While  length(Registry.ReadString('Period'+inttostr(cnt1))) > 0  do   inc(cnt1);
  SetLength(Period, cnt1);
  i1 := 0;
  While  length(Registry.ReadString('Period'+inttostr(i1))) > 0  do
  begin
   Period[i1] := Registry.ReadString('Period'+inttostr(i1));
   inc(i1);
  end;                 }


{  if length(Registry.ReadString('DATA')) > 0 then line := Registry.ReadString('DATA')
  else
  begin
  {False because we do not want to create it if it doesn�t exist}
//   if inputquery('SERVER NAME MISSING, Call Phaldi!!!','No SQL Server Specified, Please ENTER SQL Server Name : ',newvalue) then
{    begin
      Registry.WriteString('DATA',newvalue);
      Registry.WriteString('LOCAL',newlocal);
      line := Registry.ReadString('DATA');
    end;
  end;
  ADOFMSData.ConnectionString := 'Provider=SQLOLEDB.1; Persist Security Info=False; User ID=sa; Initial Catalog=FMSDATA; Data Source=' +TRIM(line);
  try
    ADOFMSData.Connected := true; }

// TEMP UNTIL ALL UPGRADED
{    Registry.WriteString('EXPORTPATH',TLOCAL['EXPORTPATH']);
    Registry.WriteString('IMPORTPATH',TLOCAL['IMPORTPATH']);
    Registry.WriteString('COUNTER',TLOCAL['COUNTER']);
    Registry.WriteString('LOADSTATUS',TLOCAL['LOADSTATUS']);
    Registry.WriteString('TRACKTYPEID',TLOCAL['TRACKTYPEID']);
    Registry.WriteString('USERID',TLOCAL['USERID']);
    Registry.WriteString('LOADID',TLOCAL['LOADID']);

}
end;

procedure TForm1.Members1Click(Sender: TObject);
var k : integer;
begin
  Members.Form2.Show;
end;

procedure TForm1.Periods1Click(Sender: TObject);
var k : integer;
begin
  periods.Form3.Show;
end;

procedure TForm1.FormClose(Sender: TObject; var Action: TCloseAction);
var k, c : integer;
begin

end;

procedure TForm1.Exit1Click(Sender: TObject);
begin
  Close;
end;

procedure TForm1.GroupBox1Click(Sender: TObject);
var k : integer;
begin

end;

procedure TForm1.Preferences1Click(Sender: TObject);
begin
  Preferences.Form4.Show;
end;

procedure TForm1.Button1Click(Sender: TObject);
var vWeekNo, vPrefno, vRand, vPeriod, i, tmember, tother, thighvalue, tlowvalue, highmember,lowmember, teller  : integer;
    vAlloc, again : Boolean;
begin
screen.Cursor := crSQLWait;
vPeriod := data.DataModule1.tPeriod['id'];
again := true;
teller := 0;

case vPeriod of
  1 :
  begin
     data.DataModule1.QStore.SQL.Text := 'update member set aweek1old = aweek1';

  end;
  2 :
  begin
     data.DataModule1.QStore.SQL.Text := 'update member set aweek2old = aweek2';
  end;
  3 :
  begin
     data.DataModule1.QStore.SQL.Text := 'update member set aweek3old = aweek3';
  end;
  4 :
  begin
     data.DataModule1.QStore.SQL.Text := 'update member set aweek4old = aweek4';
  end;
 end;
data.DataModule1.QStore.ExecSQL;



while  again and (teller < 15) do
begin
inc(teller);
data.DataModule1.qAlloc.Close;
data.DataModule1.qDone.Close;
data.DataModule1.qreport.Close;

case vPeriod of
  1 :
  begin
 //    data.DataModule1.QArchive.SQL.Text := 'update member set Aweek1old = Aweek1';
 //    data.DataModule1.QArchive.ExecSQL;
     data.DataModule1.QResetMember.SQL.Text := 'update member set Aweek1 = 0';
     data.DataModule1.QResetMember.ExecSQL;
     data.DataModule1.qAlloc.SQL.Text := 'select * from preference inner join member on member.id = preference.memberid where weekno = :pweekno and prefno = :pprefno and Aweek1 = 0';
     data.DataModule1.qDone.SQL.Text := 'select count(*) as num from member where Aweek1 = :PAweek';
     data.DataModule1.QReport.SQL.Text := 'select *, Aweek1 as weeknum from preference inner join member on member.id = preference.memberid and member.Aweek1 = preference.weekno order by Aweek1';
     data.DataModule1.QupdateMember.SQL.Text := 'update member set AWEEK1 = :pAWEEK where id = :pmemberid';
     data.DataModule1.QRevert.SQL.Text := 'update member set aweek1 = aweek1old';

  end;
  2 :
  begin
 //    data.DataModule1.QArchive.SQL.Text := 'update member set Aweek2old = Aweek2';
 //    data.DataModule1.QArchive.ExecSQL;
     data.DataModule1.QResetMember.SQL.Text := 'update member set Aweek2 = 0';
     data.DataModule1.QResetMember.ExecSQL;
     data.DataModule1.qAlloc.SQL.Text := 'select * from preference inner join member on member.id = preference.memberid where weekno = :pweekno and prefno = :pprefno and Aweek2 = 0';
     data.DataModule1.qDone.SQL.Text := 'select count(*) as num from member where Aweek2 = :PAweek';
     data.DataModule1.QReport.SQL.Text := 'select *, Aweek2 as weeknum from preference inner join member on member.id = preference.memberid and member.Aweek2 = preference.weekno order by Aweek2';
     data.DataModule1.QupdateMember.SQL.Text := 'update member set AWEEK2 = :pAWEEK where id = :pmemberid';
     data.DataModule1.QRevert.SQL.Text := 'update member set aweek2 = aweek2old';
  end;
  3 :
  begin
 //    data.DataModule1.QArchive.SQL.Text := 'update member set Aweek3old = Aweek3';
 //    data.DataModule1.QArchive.ExecSQL;
     data.DataModule1.QResetMember.SQL.Text := 'update member set Aweek3 = 0';
     data.DataModule1.QResetMember.ExecSQL;
     data.DataModule1.qAlloc.SQL.Text := 'select * from preference inner join member on member.id = preference.memberid where weekno = :pweekno and prefno = :pprefno and Aweek3 = 0';
     data.DataModule1.qDone.SQL.Text := 'select count(*) as num from member where Aweek3 = :PAweek';
     data.DataModule1.QReport.SQL.Text := 'select *, Aweek3 as weeknum from preference inner join member on member.id = preference.memberid and member.Aweek3 = preference.weekno order by Aweek3';
     data.DataModule1.QupdateMember.SQL.Text := 'update member set AWEEK3 = :pAWEEK where id = :pmemberid';
     data.DataModule1.QRevert.SQL.Text := 'update member set aweek3 = aweek3old';
  end;
  4 :
  begin
 //    data.DataModule1.QArchive.SQL.Text := 'update member set Aweek4old = Aweek4';
 //    data.DataModule1.QArchive.ExecSQL;
     data.DataModule1.QResetMember.SQL.Text := 'update member set Aweek4 = 0';
     data.DataModule1.QResetMember.ExecSQL;
     data.DataModule1.qAlloc.SQL.Text := 'select * from preference inner join member on member.id = preference.memberid where weekno = :pweekno and prefno = :pprefno and Aweek4 = 0';
     data.DataModule1.qDone.SQL.Text := 'select count(*) as num from member where Aweek4 = :PAweek';
     data.DataModule1.QReport.SQL.Text := 'select *, Aweek4 as weeknum from preference inner join member on member.id = preference.memberid and member.Aweek4 = preference.weekno order by Aweek4';
     data.DataModule1.QupdateMember.SQL.Text := 'update member set AWEEK4 = :pAWEEK where id = :pmemberid';
     data.DataModule1.QRevert.SQL.Text := 'update member set aweek4 = aweek4old';
  end;


end;

data.DataModule1.tPeriod.Locate('id',vPeriod,[]);
for i:= data.DataModule1.tPeriod['beginweek'] to data.DataModule1.tPeriod['endweek'] do
begin



end;     

 data.DataModule1.QOrder.Close;
 data.DataModule1.QOrder.Parameters.ParamByName('Pperiodid').value := vPeriod;
 data.DataModule1.QOrder.Open;
//data.DataModule1.QReset.ExecSQL;
for vPrefNo := 1 to 13 do
begin
     data.DataModule1.QOrder.First;
     while not data.DataModule1.QOrder.Eof do
     begin
//         Statusbar1.SimpleText := 'Calculating with Random Generator................ Preference No. : '+ inttostr(vPrefNo)+' and Week No. : ' +inttostr(vWeekNo);
//         application.ProcessMessages;
         data.DataModule1.qAlloc.Close;
         data.DataModule1.qAlloc.Parameters.ParamByName('pweekno').value := data.DataModule1.QOrder['weekno'];
         data.DataModule1.qAlloc.Parameters.ParamByName('pprefno').value := vPrefno;
         data.DataModule1.qAlloc.Open;
         if data.DataModule1.qAlloc.RecordCount > 0 then
         begin
           data.DataModule1.qDone.Close;
           data.DataModule1.qDone.Parameters.ParamByName('PAweek').value := data.DataModule1.QOrder['weekno'];
           data.DataModule1.qDone.Open;
           if data.DataModule1.qDone['num'] = 0 then
           begin
                vAlloc := False;

                while (vAlloc = False) do
                begin
                  if (data.DataModule1.qAlloc.locate('Memberid',random(13+1),[]))  then
                  begin
                         data.DataModule1.qUpdatemember.Parameters.ParamByName('pmemberid').value := data.DataModule1.qAlloc['memberid'];
                         data.DataModule1.qUpdatemember.Parameters.ParamByName('pAWEEK').value := data.DataModule1.QOrder['WEEKNO'];
                         data.DataModule1.qUpdatemember.ExecSQL;
                         data.DataModule1.tPreference.Requery;
                         data.DataModule1.tMember.Requery;
                         vAlloc := True
                  end;
              end; 
            end;
          end;
       Statusbar1.SimpleText := 'Calculating with Random Generator................ Preference No. : '+ inttostr(vPrefNo)+' and Week No. : ' +inttostr(Data.DataModule1.QOrder['WEEKNO']);
       application.ProcessMessages;
      data.DataModule1.QOrder.Next;
     end;

end;
//second scan
  data.DataModule1.tPreference.Refresh;
  data.DataModule1.tMember.Refresh;
 data.DataModule1.Qbadfirst.Close;
 if teller > 4 then
 begin
 case vPeriod of
  1:
   data.DataModule1.Qbadfirst.SQL.Text :='select *,Aweek1 as weekno from member inner join preference on member.Aweek1=preference.weekno and member.id=preference.memberid  where Prefno >9  ';
  2:
   data.DataModule1.Qbadfirst.SQL.Text :='select *,Aweek2 as weekno from member inner join preference on member.Aweek2=preference.weekno and member.id=preference.memberid  where Prefno >9  ';
  3:
   data.DataModule1.Qbadfirst.SQL.Text :='select *,Aweek3 as weekno from member inner join preference on member.Aweek3=preference.weekno and member.id=preference.memberid  where Prefno >9  ';
  4:
   data.DataModule1.Qbadfirst.SQL.Text :='select *,Aweek4 as weekno from member inner join preference on member.Aweek4=preference.weekno and member.id=preference.memberid  where Prefno >9  ' ;
  end;
 end
 else
 begin
 case vPeriod of
  1:
   data.DataModule1.Qbadfirst.SQL.Text :='select *,Aweek1 as weekno from member inner join preference on member.Aweek1=preference.weekno and member.id=preference.memberid  where Prefno >8  ';
  2:
   data.DataModule1.Qbadfirst.SQL.Text :='select *,Aweek2 as weekno from member inner join preference on member.Aweek2=preference.weekno and member.id=preference.memberid  where Prefno >8  ';
  3:
   data.DataModule1.Qbadfirst.SQL.Text :='select *,Aweek3 as weekno from member inner join preference on member.Aweek3=preference.weekno and member.id=preference.memberid  where Prefno >8  ';
  4:
   data.DataModule1.Qbadfirst.SQL.Text :='select *,Aweek4 as weekno from member inner join preference on member.Aweek4=preference.weekno and member.id=preference.memberid  where Prefno >8  ' ;
 end;
 end;

 data.DataModule1.Qbadfirst.Open;
 if data.DataModule1.Qbadfirst.RecordCount > 0 then
  again := true
 else
  again := false;

end;



 statusbar1.SimpleText := 'Done';
 Application.ProcessMessages;
 screen.Cursor := crDefault;
 Application.ProcessMessages;
 Form5 := TForm5.Create(Self);
 report.Form5.QuickRep1.Preview;
 Application.ProcessMessages;
 Report.Form5.Close;
 Application.ProcessMessages;
  if MessageDlg('Do you want to revert to the previous allocation rather?',
    mtConfirmation, [mbYes, mbNo], 0) = mrYes then
    begin
       data.DataModule1.QRevert.ExecSQL;
       Form5 := TForm5.Create(Self);
       report.Form5.QuickRep1.Preview;
       Application.ProcessMessages;
       Report.Form5.Close;
       Application.ProcessMessages;
    end;

end;

procedure TForm1.Button2Click(Sender: TObject);
begin
  Data.DataModule1.QReport.Close;
  Data.DataModule1.QReport.SQL.Clear;
  Data.DataModule1.QReport.SQL.Text := 'SELECT preference.WEEKNO,week.startdate, PREFNO FROM PREFERENCE inner join week on week.weekno = preference.weekno   ORDER BY week.WEEKNO, PREFNO';
  Data.DataModule1.QReport.Open;
  if Data.DataModule1.QReport.RecordCount > 0 then
  begin
    RPrefSum := TRPrefSum.Create(Self);
    RPrefSum.QRPrefSum.Preview;
    RPrefSum.Close;
  end
  else
    showmessage('NO DATA');
end;

procedure TForm1.PreferencesTemplate1Click(Sender: TObject);
var i : integer;
begin
  if MessageDlg('Are the Member table fully populated with the 13 Members?',
    mtConfirmation, [mbYes, mbNo], 0) = mrYes then
    begin
     screen.Cursor := crSQLWait;
     statusbar1.SimpleText := 'Reconstructing the template preferences table';
     data.DataModule1.QDelete.ExecSQL;
     data.DataModule1.tMember.First;
     data.DataModule1.tPeriod.Locate('id',1,[]);
     while not data.DataModule1.tMember.Eof do
     begin
       for i := data.DataModule1.tPeriod['beginweek'] to data.DataModule1.tPeriod['endweek'] do
       begin
          data.DataModule1.tPreference.append;
          data.DataModule1.tPreference['MemberID'] := data.DataModule1.tMember['id'];
          data.DataModule1.tPreference['PeriodID'] := 1;
          data.DataModule1.tPreference['WeekNo'] :=  i;
          data.DataModule1.tPreference['PrefNo'] := 13;
//          data.DataModule1.tPreference['Allocated'] := 'N';
          data.DataModule1.tPreference.Post;

       end;
       data.DataModule1.tMember.Next;
     end;


     data.DataModule1.tMember.First;
     data.DataModule1.tPeriod.Locate('id',2,[]);
     while not data.DataModule1.tMember.Eof do
     begin
       for i := data.DataModule1.tPeriod['beginweek'] to data.DataModule1.tPeriod['endweek'] do
       begin
          data.DataModule1.tPreference.append;
          data.DataModule1.tPreference['MemberID'] := data.DataModule1.tMember['id'];
          data.DataModule1.tPreference['PeriodID'] := 2;
          data.DataModule1.tPreference['WeekNo'] :=  i;
          data.DataModule1.tPreference['PrefNo'] := 13;
//          data.DataModule1.tPreference['Allocated'] := 'N';
          data.DataModule1.tPreference.Post;

       end;
       data.DataModule1.tMember.Next;
     end;

     data.DataModule1.tMember.First;
     data.DataModule1.tPeriod.Locate('id',3,[]);
     while not data.DataModule1.tMember.Eof do
     begin
       for i := data.DataModule1.tPeriod['beginweek'] to data.DataModule1.tPeriod['endweek'] do
       begin
          data.DataModule1.tPreference.append;
          data.DataModule1.tPreference['MemberID'] := data.DataModule1.tMember['id'];
          data.DataModule1.tPreference['PeriodID'] := 3;
          data.DataModule1.tPreference['WeekNo'] :=  i;
          data.DataModule1.tPreference['PrefNo'] := 13;
//          data.DataModule1.tPreference['Allocated'] := 'N';
          data.DataModule1.tPreference.Post;

       end;
       data.DataModule1.tMember.Next;
     end;

     data.DataModule1.tMember.First;
     data.DataModule1.tPeriod.Locate('id',4,[]);
     while not data.DataModule1.tMember.Eof do
     begin
       for i := data.DataModule1.tPeriod['beginweek'] to data.DataModule1.tPeriod['endweek'] do
       begin
          data.DataModule1.tPreference.append;
          data.DataModule1.tPreference['MemberID'] := data.DataModule1.tMember['id'];
          data.DataModule1.tPreference['PeriodID'] := 4;
          data.DataModule1.tPreference['WeekNo'] :=  i;
          data.DataModule1.tPreference['PrefNo'] := 13;
//          data.DataModule1.tPreference['Allocated'] := 'N';
          data.DataModule1.tPreference.Post;

       end;
       data.DataModule1.tMember.Next;
     end;
     statusbar1.SimpleText := 'Done';
     screen.Cursor := crDefault;
end;
data.DataModule1.tPeriod.First;
dblookupcombobox1.Refresh;
end;

procedure TForm1.About1Click(Sender: TObject);
begin
  about.AboutBox.Show;
end;

procedure TForm1.Button3Click(Sender: TObject);
begin
screen.Cursor := crSQLWait;
data.DataModule1.QDelTemp.ExecSQL;
data.DataModule1.tMember.First;
data.DataModule1.QITemp.SQL.Text := 'insert into temp (member, memno) values (:pmember, :pmemno) ';

while not data.DataModule1.tMember.Eof do
begin
    data.DataModule1.QITemp.Parameters.ParamByName('pmemno').value := Data.DataModule1.tmember['ID'];
    data.DataModule1.QITemp.Parameters.ParamByName('pmember').value := Data.DataModule1.tmember['ENTITYNAME'];
    data.DataModule1.QITemp.ExecSQL;
    data.DataModule1.tMember.Next;
end;

data.DataModule1.Qtemp.Close;
data.DataModule1.Qtemp.Open;
data.DataModule1.Qtemp.First;
while not data.DataModule1.Qtemp.Eof do
begin
  Data.DataModule1.QReport.Close;
  Data.DataModule1.QReport.SQL.Clear;
  Data.DataModule1.QReport.SQL.Add('SELECT *');
  Data.DataModule1.QReport.SQL.Add('FROM (member INNER JOIN preference ON (MEMBER.ID = PREFERENCE.MEMBERID)');
  Data.DataModule1.QReport.SQL.Add('and (MEMBER.aweek1 = PREFERENCE.weekno))');
  Data.DataModule1.QReport.SQL.Add('inner join week on week.weekno = preference.weekno');
  Data.DataModule1.QReport.SQL.Add('where id = :pid');
  Data.DataModule1.QReport.Parameters.ParamByName('pID').Value := Data.DataModule1.Qtemp['memno'];
  Data.DataModule1.QReport.Open;
  data.DataModule1.QITemp.SQL.Text := 'update temp set week1 = :pweek, pref1 = :ppref, weekdes = :pweekdes where memno = :pmemno ';
  data.DataModule1.QITemp.Parameters.ParamByName('pmemno').value := Data.DataModule1.QReport['ID'];
  data.DataModule1.QITemp.Parameters.ParamByName('pweek').value := Data.DataModule1.QReport['AWEEK1'];
  data.DataModule1.QITemp.Parameters.ParamByName('ppref').value := Data.DataModule1.QReport['PREFNO'];
  data.DataModule1.QITemp.Parameters.ParamByName('pweekdes').value := formatdatetime('dd/mm',Data.DataModule1.QReport['startdate']);
  data.DataModule1.QITemp.ExecSQL;

  data.DataModule1.Qtemp.Next;
end;
data.DataModule1.Qtemp.First;

while not data.DataModule1.Qtemp.Eof do
begin
  Data.DataModule1.QReport.Close;
  Data.DataModule1.QReport.SQL.Clear;
  Data.DataModule1.QReport.SQL.Add('SELECT *');
  Data.DataModule1.QReport.SQL.Add('FROM (member INNER JOIN preference ON (MEMBER.ID = PREFERENCE.MEMBERID)');
  Data.DataModule1.QReport.SQL.Add('and (MEMBER.aweek2 = PREFERENCE.weekno))');
  Data.DataModule1.QReport.SQL.Add('inner join week on week.weekno = preference.weekno');
  Data.DataModule1.QReport.SQL.Add('where id = :pid');
  Data.DataModule1.QReport.Parameters.ParamByName('pID').Value := Data.DataModule1.Qtemp['memno'];
  Data.DataModule1.QReport.Open;
  data.DataModule1.QITemp.SQL.Text := 'update temp set week2 = :pweek, pref2 = :ppref, weekdes = :pweekdes  where memno = :pmemno ';
  data.DataModule1.QITemp.Parameters.ParamByName('pmemno').value := Data.DataModule1.QReport['MEMBERID'];
  data.DataModule1.QITemp.Parameters.ParamByName('pweek').value := Data.DataModule1.QReport['AWEEK2'];
  data.DataModule1.QITemp.Parameters.ParamByName('ppref').value := Data.DataModule1.QReport['PREFNO'];
  data.DataModule1.QITemp.Parameters.ParamByName('pweekdes').value := formatdatetime('dd/mm',Data.DataModule1.QReport['startdate']);
  data.DataModule1.QITemp.ExecSQL;

  data.DataModule1.Qtemp.Next;
end;
data.DataModule1.Qtemp.First;

while not data.DataModule1.Qtemp.Eof do
begin
  Data.DataModule1.QReport.Close;
  Data.DataModule1.QReport.SQL.Clear;
  Data.DataModule1.QReport.SQL.Add('SELECT *');
  Data.DataModule1.QReport.SQL.Add('FROM (member INNER JOIN preference ON (MEMBER.ID = PREFERENCE.MEMBERID)');
  Data.DataModule1.QReport.SQL.Add('and (MEMBER.aweek3 = PREFERENCE.weekno))');
  Data.DataModule1.QReport.SQL.Add('inner join week on week.weekno = preference.weekno');
  Data.DataModule1.QReport.SQL.Add('where id = :pid');
  Data.DataModule1.QReport.Parameters.ParamByName('pID').Value := Data.DataModule1.Qtemp['memno'];
  Data.DataModule1.QReport.Open;
  data.DataModule1.QITemp.SQL.Text := 'update temp set week3 = :pweek, pref3 = :ppref, weekdes = :pweekdes  where memno = :pmemno ';
  data.DataModule1.QITemp.Parameters.ParamByName('pmemno').value := Data.DataModule1.QReport['MEMBERID'];
  data.DataModule1.QITemp.Parameters.ParamByName('pweek').value := Data.DataModule1.QReport['AWEEK3'];
  data.DataModule1.QITemp.Parameters.ParamByName('ppref').value := Data.DataModule1.QReport['PREFNO'];
  data.DataModule1.QITemp.Parameters.ParamByName('pweekdes').value := formatdatetime('dd/mm',Data.DataModule1.QReport['startdate']);
  data.DataModule1.QITemp.ExecSQL;

  data.DataModule1.Qtemp.Next;
end;

data.DataModule1.Qtemp.First;

while not data.DataModule1.Qtemp.Eof do
begin
  Data.DataModule1.QReport.Close;
  Data.DataModule1.QReport.SQL.Clear;
  Data.DataModule1.QReport.SQL.Add('SELECT *');
  Data.DataModule1.QReport.SQL.Add('FROM (member INNER JOIN preference ON (MEMBER.ID = PREFERENCE.MEMBERID)');
  Data.DataModule1.QReport.SQL.Add('and (MEMBER.aweek4 = PREFERENCE.weekno))');
  Data.DataModule1.QReport.SQL.Add('inner join week on week.weekno = preference.weekno');
  Data.DataModule1.QReport.SQL.Add('where id = :pid');
  Data.DataModule1.QReport.Parameters.ParamByName('pID').Value := Data.DataModule1.Qtemp['memno'];
  Data.DataModule1.QReport.Open;
  data.DataModule1.QITemp.SQL.Text := 'update temp set week4 = :pweek, pref4 = :ppref, weekdes = :pweekdes  where memno = :pmemno ';
  data.DataModule1.QITemp.Parameters.ParamByName('pmemno').value := Data.DataModule1.QReport['MEMBERID'];
  data.DataModule1.QITemp.Parameters.ParamByName('pweek').value := Data.DataModule1.QReport['AWEEK4'];
  data.DataModule1.QITemp.Parameters.ParamByName('ppref').value := Data.DataModule1.QReport['PREFNO'];
  data.DataModule1.QITemp.Parameters.ParamByName('pweekdes').value := formatdatetime('dd/mm',Data.DataModule1.QReport['startdate']);
  data.DataModule1.QITemp.ExecSQL;

  data.DataModule1.Qtemp.Next;
end;
data.DataModule1.Qtemp.CLose;
data.DataModule1.Qtemp.open;
RAllocSUM := TRAllocSUM.Create(Self);
screen.Cursor := crDefault;
allocsum.RAllocSum.QRAllocSum.Preview;
allocsum.RAllocSum.Close;
end;

procedure TForm1.Button4Click(Sender: TObject);
begin
screen.Cursor := crSQLWait;
data.DataModule1.QDelTemp.ExecSQL;
data.DataModule1.QITemp.SQL.Text := 'insert into temp (member, memno,week1,pref1, weekdes) values (:pmember, :pmemno, :pweek, :ppref, :pweekdes) ';

  Data.DataModule1.QReport.Close;
  Data.DataModule1.QReport.SQL.Clear;
  Data.DataModule1.QReport.SQL.Add('SELECT id, entityname, preference.weekno,prefno,startdate, startdate+6  as enddate');
  Data.DataModule1.QReport.SQL.Add('FROM Week INNER JOIN (member INNER JOIN preference ON (member.Aweek1 = preference.WeekNo) AND (member.ID = preference.MemberID)) ON Week.WEEKNO = preference.WeekNo');
  Data.DataModule1.QReport.SQL.Add('ORDER BY preference.WeekNo;');

  Data.DataModule1.QReport.Open;


while not data.DataModule1.Qreport.Eof do
begin
    data.DataModule1.QITemp.Parameters.ParamByName('pmemno').value := Data.DataModule1.Qreport['ID'];
    data.DataModule1.QITemp.Parameters.ParamByName('pmember').value := Data.DataModule1.Qreport['ENTITYNAME'];
    data.DataModule1.QITemp.Parameters.ParamByName('pweek').value := Data.DataModule1.Qreport['weekno'];
    data.DataModule1.QITemp.Parameters.ParamByName('ppref').value := Data.DataModule1.Qreport['prefno'];
    data.DataModule1.QITemp.Parameters.ParamByName('pweekdes').value := formatdatetime('DD-MMM-YYYY',Data.DataModule1.QReport['STARTDATE']) +' to ' +formatdatetime('DD-MMM-YYYY',Data.DataModule1.QReport['endDATE']);
    data.DataModule1.QITemp.ExecSQL;
    data.DataModule1.Qreport.Next;
end;

////2
  Data.DataModule1.QReport.Close;
  Data.DataModule1.QReport.SQL.Clear;
  Data.DataModule1.QReport.SQL.Add('SELECT id, entityname, preference.weekno,prefno,startdate, startdate+6  as enddate');
  Data.DataModule1.QReport.SQL.Add('FROM Week INNER JOIN (member INNER JOIN preference ON (member.Aweek2 = preference.WeekNo) AND (member.ID = preference.MemberID)) ON Week.WEEKNO = preference.WeekNo');
  Data.DataModule1.QReport.SQL.Add('ORDER BY preference.WeekNo;');

  Data.DataModule1.QReport.Open;


while not data.DataModule1.Qreport.Eof do
begin
    data.DataModule1.QITemp.Parameters.ParamByName('pmemno').value := Data.DataModule1.Qreport['ID'];
    data.DataModule1.QITemp.Parameters.ParamByName('pmember').value := Data.DataModule1.Qreport['ENTITYNAME'];
    data.DataModule1.QITemp.Parameters.ParamByName('pweek').value := Data.DataModule1.Qreport['weekno'];
    data.DataModule1.QITemp.Parameters.ParamByName('ppref').value := Data.DataModule1.Qreport['prefno'];
    data.DataModule1.QITemp.Parameters.ParamByName('pweekdes').value := formatdatetime('DD-MMM-YYYY',Data.DataModule1.QReport['STARTDATE']) +' to ' +formatdatetime('DD-MMM-YYYY',Data.DataModule1.QReport['endDATE']);
    data.DataModule1.QITemp.ExecSQL;
    data.DataModule1.Qreport.Next;
end;

/////3
  Data.DataModule1.QReport.Close;
  Data.DataModule1.QReport.SQL.Clear;
  Data.DataModule1.QReport.SQL.Add('SELECT id, entityname, preference.weekno,prefno,startdate, startdate+6  as enddate');
  Data.DataModule1.QReport.SQL.Add('FROM Week INNER JOIN (member INNER JOIN preference ON (member.Aweek3 = preference.WeekNo) AND (member.ID = preference.MemberID)) ON Week.WEEKNO = preference.WeekNo');
  Data.DataModule1.QReport.SQL.Add('ORDER BY preference.WeekNo;');

  Data.DataModule1.QReport.Open;


while not data.DataModule1.Qreport.Eof do
begin
    data.DataModule1.QITemp.Parameters.ParamByName('pmemno').value := Data.DataModule1.Qreport['ID'];
    data.DataModule1.QITemp.Parameters.ParamByName('pmember').value := Data.DataModule1.Qreport['ENTITYNAME'];
    data.DataModule1.QITemp.Parameters.ParamByName('pweek').value := Data.DataModule1.Qreport['weekno'];
    data.DataModule1.QITemp.Parameters.ParamByName('ppref').value := Data.DataModule1.Qreport['prefno'];
    data.DataModule1.QITemp.Parameters.ParamByName('pweekdes').value := formatdatetime('DD-MMM-YYYY',Data.DataModule1.QReport['STARTDATE']) +' to ' +formatdatetime('DD-MMM-YYYY',Data.DataModule1.QReport['endDATE']);
    data.DataModule1.QITemp.ExecSQL;
    data.DataModule1.Qreport.Next;
end;

/////4

  Data.DataModule1.QReport.Close;
  Data.DataModule1.QReport.SQL.Clear;
  Data.DataModule1.QReport.SQL.Add('SELECT id, entityname, preference.weekno,prefno,startdate, startdate+6  as enddate');
  Data.DataModule1.QReport.SQL.Add('FROM Week INNER JOIN (member INNER JOIN preference ON (member.Aweek4 = preference.WeekNo) AND (member.ID = preference.MemberID)) ON Week.WEEKNO = preference.WeekNo');
  Data.DataModule1.QReport.SQL.Add('ORDER BY preference.WeekNo;');

  Data.DataModule1.QReport.Open;


while not data.DataModule1.Qreport.Eof do
begin
    data.DataModule1.QITemp.Parameters.ParamByName('pmemno').value := Data.DataModule1.Qreport['ID'];
    data.DataModule1.QITemp.Parameters.ParamByName('pmember').value := Data.DataModule1.Qreport['ENTITYNAME'];
    data.DataModule1.QITemp.Parameters.ParamByName('pweek').value := Data.DataModule1.Qreport['weekno'];
    data.DataModule1.QITemp.Parameters.ParamByName('ppref').value := Data.DataModule1.Qreport['prefno'];
    data.DataModule1.QITemp.Parameters.ParamByName('pweekdes').value := formatdatetime('DD-MMM-YYYY',Data.DataModule1.QReport['STARTDATE']) +' to ' +formatdatetime('DD-MMM-YYYY',Data.DataModule1.QReport['endDATE']);
    data.DataModule1.QITemp.ExecSQL;
    data.DataModule1.Qreport.Next;
end;

    data.DataModule1.Qtemp.Close;
    data.DataModule1.Qtemp.Open;

    screen.Cursor := crDefault;
    RWeekMember := TRWeekMember.Create(Self);
    RWeekMember.QRWeekMember.Preview;
    RWeekMember.Close;
    data.DataModule1.Qtemp.Close;
//////////END


                     

//nbd
end;

procedure TForm1.SetWeeks;
var tt : integer; STARTDATE : String;
begin
  STARTDATE := '28/12/2000';
  Data.DataModule1.QReport.Close;
  Data.DataModule1.QReport.SQL.Clear;
  Data.DataModule1.QReport.SQL.Add('INSERT INTO WEEK (WEEKNO, STARTDATE) VALUES (:pWEEKNO, :pSTARTDATE)');
  for tt := 1 to 52 do
  begin
    Data.DataModule1.QReport.Parameters.ParamByName('pWEEKNO').Value := tt;
    Data.DataModule1.QReport.Parameters.ParamByName('pSTARTDATE').Value := strtodate(formatdatetime('DD/MM/YYYY',strtodate(STARTDATE) +(tt *7)));
    Data.DataModule1.QReport.ExecSQL;
  end;
end;

procedure TForm1.Button5Click(Sender: TObject);
var fl : textfile;
    I : INTEGER;
begin
if sd.Execute then
begin
 data.DataModule1.tMember.First;
 while not data.DataModule1.tMember.Eof do
 begin
   assignfile(fl,extractfilepath(sd.filename)+'\TAMBOTI_MEMBER'+TRIM(INTTOSTR(DATA.DataModule1.tMember['ID']))+'.CSV');
   REWRITE(FL);
   WRITELN(FL,'TAMBOTIE LODGE - '+DATA.DataModule1.tMember['ENTITYNAME']);
   WRITELN(FL,'Week Allocation Request Form'+',,,'+'for JAN to DEC '+formatdatetime('yyyy',DataModule1.tweek['startdate'])  );
   WRITELN(FL,',');
   data.DataModule1.tweek.First;
   writeln(fl,'WEEK NUMBER,,,,PREF(1-13)');
   while not data.DataModule1.tweek.Eof do
   begin
    if (data.DataModule1.tweek['weekno']-1) mod 13 = 0 then
     writeln(fl,'Quarter '+floattostr((data.DataModule1.tweek['weekno']-1)/13+1));
    writeln(fl,TRIM(INTTOSTR(DataModule1.tweek['WEEKNO']))+',('+formatdatetime('dd-mmm',DataModule1.tweek['startdate'])+' to '+formatdatetime('dd-mmm',DataModule1.tweek['startdate']+6)+')');
    data.DataModule1.tweek.Next;
   end;


   data.DataModule1.tMember.Next;
   writeln(fl,',');
   writeln(fl,'Note:'+','+'Please Return to Administration before '+formatdatetime('dd-mmm-yyyy',now+30));
//   writeln(fl,','+'Place one X per week maximum');
 closefile(fl);
 end;
 showmessage('Files were created in '+extractfilepath(sd.filename)+' for each member!');
end;

end;

procedure TForm1.Button6Click(Sender: TObject);
begin
    screen.Cursor := crSQlWait;
    data.DataModule1.QReport.Close;
    data.DataModule1.QReport.SQL.Text := 'select * from (preference inner join member on member.id=preference.memberid) inner join week on week.weekno=preference.weekno order by member.id,preference.weekno';
    data.DataModule1.QReport.Open;
    screen.Cursor := crDefault;
    RPrefTot := TRPrefTot.Create(Self);
    RPrefTot.QRPrefTot.Preview;
    RPrefTot.Close;
    data.DataModule1.QReport.Close;

end;

procedure TForm1.Button7Click(Sender: TObject);

function getcomma(sin:string; pos : integer):string;
var i,j,h : integer;
 
begin

  i:=1;
  j:=0;
  h:=1;
 while j < pos do
 begin
  h:=i;
  while (sin[i] <> ',') and  (sin[i]<>#13) do
   begin
    inc(i);
   end;
   inc(j);
   inc(i);
 
  end;
 
  if pos <> 1 then
    getcomma := copy(sin,h,i-h-1)
  else
    getcomma := copy(sin,1,i-2);
 
end;


var fl : textfile; line : string;
vPeriodID,vMemberID,vPrefNo,vWeekno : Integer;
begin
if od2.Execute then
begin
 assignfile(fl,od2.filename);
 reset(fl);
vMemberID :=  strtoint(copy(extractfilename(od2.filename),15,1));

 while not eof(fl) do
 begin
   readln(fl,line);
   if length(line) = 0 then readln(fl,line);
    if copy(getcomma(line,1),1,1) = 'Q' then
    begin
      vPeriodID := strtoint(copy(getcomma(line,1),9,1));
    end
    else
     if (length(getcomma(line,2)) > 0) and (length(getcomma(line,5)) > 0) then
    begin
      vweekNo := strtoint(getcomma(line,1));
      vPrefNo := strtoint(getcomma(line,5));
      data.DataModule1.QUpdatePref.Parameters.ParamByName('pmemberID').value := vMemberID;
      data.DataModule1.QUpdatePref.Parameters.ParamByName('pWeekNo').value := vWeekNo;
      data.DataModule1.QUpdatePref.Parameters.ParamByName('pPrefNo').value := vPrefNo;
      data.DataModule1.QUpdatePref.ExecSQL;
    end;



 end;
 showmessage('Done processing file - '+od2.FileName);
end;
end;

end.
