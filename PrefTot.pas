unit PrefTot;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, QuickRpt, Qrctrls;

type
  TRPrefTot = class(TForm)
    QRPrefTot: TQuickRep;
    QRBand1: TQRBand;
    QRSysData1: TQRSysData;
    QRLabel17: TQRLabel;
    QRBand2: TQRBand;
    QRLabel1: TQRLabel;
    QRLabel3: TQRLabel;
    QRBand3: TQRBand;
    QRDBText2: TQRDBText;
    QRImage1: TQRImage;
    QRLabel2: TQRLabel;
    QRDBText3: TQRDBText;
    QRGroup1: TQRGroup;
    QRDBText1: TQRDBText;
    L1: TQRLabel;
    QRDBText4: TQRDBText;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QRBand3BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRPrefTotBeforePrint(Sender: TCustomQuickRep;
      var PrintReport: Boolean);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  RPrefTot: TRPrefTot;

implementation

uses data;

{$R *.DFM}

procedure TRPrefTot.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action := caFree;
end;

procedure TRPrefTot.QRBand3BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
 IF DATA.DataModule1.QReport['PREFERENCE.WEEKNO'] MOD 13 = 1 THEN
 begin
  qrbAND3.Frame.Width := 2;
  L1.Caption := 'Quarter ' + inttostr(DATA.DataModule1.QReport['periodid']);
 end
 ELSE
 begin
  qrbAND3.Frame.Width := 1;
  L1.Caption := '';
 end; 

end;

procedure TRPrefTot.QRPrefTotBeforePrint(Sender: TCustomQuickRep;
  var PrintReport: Boolean);
begin
   QRImage1.Picture.LoadFromFile('logo.bmp');
end;

end.
