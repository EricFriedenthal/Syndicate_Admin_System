unit data;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, ADODB;

type
  TDataModule1 = class(TDataModule)
    SyndicateDB: TADOConnection;
    tMember: TADOTable;
    tPeriod: TADOTable;
    tPreference: TADOTable;
    dsMember: TDataSource;
    dsPeriod: TDataSource;
    dsPreference: TDataSource;
    tPreferenceMemberID: TSmallintField;
    tPreferencePeriodID: TSmallintField;
    tPreferenceWeekNo: TSmallintField;
    tPreferencePrefNo: TSmallintField;
    tPreferenceluPeriod: TStringField;
    qAlloc: TADOQuery;
    qUpdateAlloc: TADOQuery;
    QDecline: TADOQuery;
    QOrder: TADOQuery;
    QReport: TADOQuery;
    QDelete: TADOQuery;
    QupdateMember: TADOQuery;
    QResetMember: TADOQuery;
    QRevert: TADOQuery;
    QArchive: TADOQuery;
    QDone: TADOQuery;
    QMEMPREF: TADOQuery;
    dsMEMPREF: TDataSource;
    QUPD1: TADOQuery;
    QUPD2: TADOQuery;
    Qbadfirst: TADOQuery;
    Qtemp: TADOQuery;
    QDelTemp: TADOQuery;
    QITemp: TADOQuery;
    Qperiod: TADOQuery;
    QMU: TADOQuery;
    QRepMember: TADOQuery;
    tweek: TADOTable;
    QStore: TADOQuery;
    dsweek: TDataSource;
    QUpdatePref: TADOQuery;
    procedure tPreferenceAfterInsert(DataSet: TDataSet);
    procedure tPreferencePostError(DataSet: TDataSet; E: EDatabaseError;
      var Action: TDataAction);
    procedure tPreferenceBeforeScroll(DataSet: TDataSet);
    procedure tPreferenceAfterPost(DataSet: TDataSet);
    procedure DataModuleCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  DataModule1: TDataModule1;

implementation

uses Main;

{$R *.DFM}

procedure TDataModule1.tPreferenceAfterInsert(DataSet: TDataSet);
begin
//  Tpreference.Refresh;
end;

procedure TDataModule1.tPreferencePostError(DataSet: TDataSet;
  E: EDatabaseError; var Action: TDataAction);
begin
  showmessage('Invalid or Duplicate Week entered, try again! ');
  tpreference.CancelUpdates;
  Action := daAbort;
end;

procedure TDataModule1.tPreferenceBeforeScroll(DataSet: TDataSet);
begin
 // tpreference.Last;

end;

procedure TDataModule1.tPreferenceAfterPost(DataSet: TDataSet);
begin
   if tpreference['periodid'] = null then showmessage('Select Period');
  if tpreference['prefno'] = null then showmessage('Enter Preference eg 1,2,3 etc');
  if tperiod.Locate('id',tpreference['periodid'],[]) then
   if (tpreference['weekno'] < tperiod['beginweek']) or (tpreference['weekno'] > tperiod['endweek']) then
     showmessage('Week not in selected Period');

end;

procedure TDataModule1.DataModuleCreate(Sender: TObject);
begin
  SyndicateDB.connected := False;
  if main.Form1.od.Execute then
  begin
  try
    main.Form1.Image1.Picture.LoadFromFile('logo.bmp');
    main.Form1.Caption := main.Form1.od.FileName;
    SyndicateDB.ConnectionString :=  'Provider=Microsoft.Jet.OLEDB.4.0;Password="";User ID=Admin;Jet OLEDB:Database Password="";Jet OLEDB:Engine Type=5;Jet OLEDB:Database Locking Mode=1;Jet OLEDB:New Database Password="";Jet OLEDB:Create System Database=False;Jet OLEDB:SFP=False;Data Source='+  main.Form1.od.FileName;
    SyndicateDB.connected := True;
    tmember.Open;
    tweek.Open;
    tpreference.Open;
    tperiod.Open;
    main.Form1.BringToFront;
  //C:\Program Files\Syndicate\Data\Syndicate.mdb
  except
   showmessage('Wrong database selection');
   application.Terminate;
  end;
  end
  else
  begin
      showmessage('Wrong database selection');
      application.Terminate;
  end;

end;

end.
