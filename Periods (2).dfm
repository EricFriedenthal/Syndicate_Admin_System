object Form3: TForm3
  Left = 3
  Top = 137
  Width = 676
  Height = 354
  Caption = 'frmPeriods'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object Label3: TLabel
    Left = 432
    Top = 176
    Width = 160
    Height = 13
    Caption = 'Use this to lookup week numbers '
  end
  object Label1: TLabel
    Left = 429
    Top = 208
    Width = 207
    Height = 16
    Caption = '(Select Begin date of Week 1)'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object MonthCalendar1: TMonthCalendar
    Left = 416
    Top = 16
    Width = 225
    Height = 154
    MultiSelect = True
    Date = 37076
    EndDate = 37076
    FirstDayOfWeek = dowThursday
    MaxSelectRange = 365
    ParentShowHint = False
    ShowHint = True
    TabOrder = 0
    WeekNumbers = True
  end
  object DBGrid1: TDBGrid
    Left = 0
    Top = 0
    Width = 385
    Height = 327
    Align = alLeft
    DataSource = DataModule1.dsPeriod
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    Columns = <
      item
        Expanded = False
        FieldName = 'ID'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Description'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'BeginWeek'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'EndWeek'
        Visible = True
      end>
  end
  object Button1: TButton
    Left = 432
    Top = 230
    Width = 201
    Height = 57
    Caption = 'Update Week No Start Dates'
    TabOrder = 2
    OnClick = Button1Click
  end
end
