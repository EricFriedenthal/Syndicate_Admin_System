unit WeekMember;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Qrctrls, QuickRpt, ExtCtrls;

type
  TRWeekMember = class(TForm)
    QRWeekMember: TQuickRep;
    QRBand1: TQRBand;
    QRSysData1: TQRSysData;
    QRLabel17: TQRLabel;
    QRBand2: TQRBand;
    QRLabel1: TQRLabel;
    QRLabel2: TQRLabel;
    QRLabel3: TQRLabel;
    QRBand3: TQRBand;
    QRDBText1: TQRDBText;
    QRDBText2: TQRDBText;
    QRDBText3: TQRDBText;
    QRDBText4: TQRDBText;
    QRLabel4: TQRLabel;
    QRImage1: TQRImage;
    procedure QRBand3BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  RWeekMember: TRWeekMember;

implementation

uses data;

{$R *.DFM}

procedure TRWeekMember.QRBand3BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
{ FTDATE.Caption := formatdatetime('DD-MMM-YYYY',Data.DataModule1.QReport['STARTDATE']) +' to ' +formatdatetime('DD-MMM-YYYY',Data.DataModule1.QReport['STARTDATE'] +7);
  try
    ENTNAME.Caption := trim(Data.DataModule1.QReport['ENTITYNAME']);
  except
    ENTNAME.Caption := '';
  end;
  try
    ENTNAME.Caption := ENTNAME.Caption +' - ' +trim(Data.DataModule1.QReport['CONTACTPERSON']);
  except
    ENTNAME.Caption := ENTNAME.Caption;
  end;        }
end;

procedure TRWeekMember.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := caFree;
end;

end.
