unit AllocSum;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Qrctrls, QuickRpt, ExtCtrls;

type
  TRAllocSum = class(TForm)
    QRAllocSum: TQuickRep;
    QRBand1: TQRBand;
    QRSysData1: TQRSysData;
    QRLabel17: TQRLabel;
    QRBand2: TQRBand;
    QRLabel1: TQRLabel;
    QRLabel2: TQRLabel;
    QRBand3: TQRBand;
    QRLabel3: TQRLabel;
    QRDBText5: TQRDBText;
    QRDBText6: TQRDBText;
    QRDBText7: TQRDBText;
    QRDBText8: TQRDBText;
    QRLabel4: TQRLabel;
    QRLabel5: TQRLabel;
    QRLabel6: TQRLabel;
    QRLabel7: TQRLabel;
    QRLabel8: TQRLabel;
    QRLabel9: TQRLabel;
    QRLabel10: TQRLabel;
    QRLabel11: TQRLabel;
    QRLabel12: TQRLabel;
    QRLabel13: TQRLabel;
    QRDBText1: TQRDBText;
    QRDBText2: TQRDBText;
    QRDBText3: TQRDBText;
    QRDBText4: TQRDBText;
    QRDBText9: TQRDBText;
    QRDBText10: TQRDBText;
    QRShape1: TQRShape;
    QRShape2: TQRShape;
    QRShape3: TQRShape;
    QRShape4: TQRShape;
    QRShape6: TQRShape;
    QRShape7: TQRShape;
    QRShape8: TQRShape;
    QRShape9: TQRShape;
    QRShape10: TQRShape;
    QRShape11: TQRShape;
    QRShape12: TQRShape;
    QRShape13: TQRShape;
    QRShape14: TQRShape;
    QRShape16: TQRShape;
    QRShape17: TQRShape;
    QRShape18: TQRShape;
    QRShape19: TQRShape;
    QRImage1: TQRImage;
    MEMO: TQRMemo;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QRAllocSumBeforePrint(Sender: TCustomQuickRep;
      var PrintReport: Boolean);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  RAllocSum: TRAllocSum;

implementation

uses data;

{$R *.DFM}

procedure TRAllocSum.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action := caFree;
end;

procedure TRAllocSum.QRAllocSumBeforePrint(Sender: TCustomQuickRep;
  var PrintReport: Boolean);
  var line : string;
begin
  memo.Lines.Clear;
  memo.Lines.Add('Week numbers Associated with Startdates');
  memo.Lines.Add('***************************************');

  datamodule1.tweek.First;
  while not datamodule1.tweek.Eof do
  begin
  line := formatfloat('00',datamodule1.tweek['weekno'])+'  -  '+formatdatetime('dd-mmm-yyyy',datamodule1.tweek['startdate'])+'        ' ;
    datamodule1.tweek.Next;
  line := line + formatfloat('00',datamodule1.tweek['weekno'])+'  -  '+formatdatetime('dd-mmm-yyyy',datamodule1.tweek['startdate'] );
    memo.Lines.Add(line);
    datamodule1.tweek.Next;
  end;
  QRImage1.Picture.LoadFromFile('logo.bmp');
end;

end.
