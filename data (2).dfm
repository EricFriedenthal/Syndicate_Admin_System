object DataModule1: TDataModule1
  OldCreateOrder = False
  Left = 128
  Top = 153
  Height = 510
  Width = 745
  object SyndicateDB: TADOConnection
    Connected = True
    ConnectionString = 
      'Provider=Microsoft.Jet.OLEDB.4.0;Password="";User ID=Admin;Data ' +
      'Source=C:\Program Files\Syndicate\Data\Syndicate.mdb;Mode=Share ' +
      'Deny None;Extended Properties="";Locale Identifier=1033;Jet OLED' +
      'B:System database="";Jet OLEDB:Registry Path="";Jet OLEDB:Databa' +
      'se Password="";Jet OLEDB:Engine Type=5;Jet OLEDB:Database Lockin' +
      'g Mode=1;Jet OLEDB:Global Partial Bulk Ops=2;Jet OLEDB:Global Bu' +
      'lk Transactions=1;Jet OLEDB:New Database Password="";Jet OLEDB:C' +
      'reate System Database=False;Jet OLEDB:Encrypt Database=False;Jet' +
      ' OLEDB:Don'#39't Copy Locale on Compact=False;Jet OLEDB:Compact With' +
      'out Replica Repair=False;Jet OLEDB:SFP=False'
    LoginPrompt = False
    Mode = cmShareDenyNone
    Provider = 'Microsoft.Jet.OLEDB.4.0'
    Left = 56
    Top = 24
  end
  object tMember: TADOTable
    Active = True
    Connection = SyndicateDB
    CursorType = ctStatic
    TableName = 'Member'
    Left = 184
    Top = 280
  end
  object tPeriod: TADOTable
    Active = True
    Connection = SyndicateDB
    CursorType = ctStatic
    IndexFieldNames = 'ID'
    TableName = 'Periods'
    Left = 56
    Top = 240
  end
  object tPreference: TADOTable
    Active = True
    Connection = SyndicateDB
    CursorType = ctStatic
    AfterInsert = tPreferenceAfterInsert
    AfterPost = tPreferenceAfterPost
    BeforeScroll = tPreferenceBeforeScroll
    OnPostError = tPreferencePostError
    IndexFieldNames = 'MemberID;PeriodID'
    MasterFields = 'MEMBERID;PERIODID'
    MasterSource = dsMEMPREF
    TableName = 'Preference'
    Left = 96
    Top = 168
    object tPreferenceMemberID: TSmallintField
      FieldName = 'MemberID'
    end
    object tPreferencePeriodID: TSmallintField
      FieldName = 'PeriodID'
    end
    object tPreferenceWeekNo: TSmallintField
      FieldName = 'WeekNo'
    end
    object tPreferencePrefNo: TSmallintField
      FieldName = 'PrefNo'
    end
    object tPreferenceluPeriod: TStringField
      FieldKind = fkLookup
      FieldName = 'luPeriod'
      LookupDataSet = tPeriod
      LookupKeyFields = 'ID'
      LookupResultField = 'Description'
      KeyFields = 'PeriodID'
      Lookup = True
    end
  end
  object dsMember: TDataSource
    DataSet = tMember
    Left = 184
    Top = 112
  end
  object dsPeriod: TDataSource
    DataSet = tPeriod
    Left = 128
    Top = 264
  end
  object dsPreference: TDataSource
    DataSet = tPreference
    Left = 352
    Top = 64
  end
  object qAlloc: TADOQuery
    Connection = SyndicateDB
    Parameters = <
      item
        Name = 'pweekno'
        Attributes = [paNullable]
        DataType = ftFixedChar
        NumericScale = 255
        Precision = 255
        Size = 510
        Value = Null
      end
      item
        Name = 'pprefno'
        Attributes = [paNullable]
        DataType = ftFixedChar
        NumericScale = 255
        Precision = 255
        Size = 510
        Value = Null
      end>
    SQL.Strings = (
      
        'select * from preference inner join member on member.id = prefer' +
        'ence.memberid'
      'where weekno = :pweekno and prefno = :pprefno and Aweek1 <> 0')
    Left = 360
    Top = 192
  end
  object qUpdateAlloc: TADOQuery
    Connection = SyndicateDB
    Parameters = <
      item
        Name = 'pweekno'
        Attributes = [paNullable]
        DataType = ftFixedChar
        NumericScale = 255
        Precision = 255
        Size = 510
        Value = Null
      end
      item
        Name = 'pmemberid'
        Attributes = [paNullable]
        DataType = ftFixedChar
        NumericScale = 255
        Precision = 255
        Size = 510
        Value = Null
      end>
    SQL.Strings = (
      'update MEMBER set Aweek1 = :pWEEKNO'
      'where memberid = :pmemberid')
    Left = 352
    Top = 256
  end
  object QDecline: TADOQuery
    Connection = SyndicateDB
    Parameters = <
      item
        Name = 'Pweekno'
        Attributes = [paNullable]
        DataType = ftFixedChar
        NumericScale = 255
        Precision = 255
        Size = 510
        Value = Null
      end>
    SQL.Strings = (
      'update preference set Allocated2 = '#39'D'#39' where weekno=:Pweekno')
    Left = 344
    Top = 328
  end
  object QOrder: TADOQuery
    Connection = SyndicateDB
    Parameters = <
      item
        Name = 'Pperiodid'
        Attributes = [paNullable]
        DataType = ftFixedChar
        NumericScale = 255
        Precision = 255
        Size = 510
        Value = Null
      end>
    SQL.Strings = (
      
        'select weekno, sum(prefno) from preference where periodid = :Ppe' +
        'riodid '
      'group  by weekno order by sum(prefno)  desc')
    Left = 408
    Top = 312
  end
  object QReport: TADOQuery
    Connection = SyndicateDB
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'SELECT WEEKNO, PREFNO'
      'FROM PREFERENCE'
      'ORDER BY WEEKNO, PREFNO')
    Left = 248
    Top = 304
  end
  object QDelete: TADOQuery
    Connection = SyndicateDB
    Parameters = <>
    SQL.Strings = (
      'delete from preference')
    Left = 248
    Top = 16
  end
  object QupdateMember: TADOQuery
    Connection = SyndicateDB
    Parameters = <
      item
        Name = 'pAWEEK1'
        Attributes = [paNullable]
        DataType = ftFixedChar
        NumericScale = 255
        Precision = 255
        Size = 510
        Value = Null
      end
      item
        Name = 'pmemberid'
        Attributes = [paNullable]
        DataType = ftFixedChar
        NumericScale = 255
        Precision = 255
        Size = 510
        Value = Null
      end>
    SQL.Strings = (
      'update member set AWEEK1 = :pAWEEK1 where id = :pmemberid')
    Left = 344
    Top = 136
  end
  object QResetMember: TADOQuery
    Connection = SyndicateDB
    Parameters = <>
    SQL.Strings = (
      'update member set Aperiod=0')
    Left = 56
    Top = 328
  end
  object QRevert: TADOQuery
    Connection = SyndicateDB
    Parameters = <>
    SQL.Strings = (
      'update preference set allocated2 = allocated1')
    Left = 160
    Top = 344
  end
  object QArchive: TADOQuery
    Connection = SyndicateDB
    Parameters = <>
    SQL.Strings = (
      'update preference set allocated1 = allocated2')
    Left = 152
    Top = 184
  end
  object QDone: TADOQuery
    Connection = SyndicateDB
    Parameters = <
      item
        Name = 'PAweek1'
        Attributes = [paNullable]
        DataType = ftFixedChar
        NumericScale = 255
        Precision = 255
        Size = 510
        Value = Null
      end>
    SQL.Strings = (
      'select count(*) as num from member where Aweek1 = :PAweek1')
    Left = 56
    Top = 112
  end
  object QMEMPREF: TADOQuery
    Connection = SyndicateDB
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'pMEMBERID'
        Attributes = [paNullable]
        DataType = ftFixedChar
        NumericScale = 255
        Precision = 255
        Size = 510
        Value = Null
      end>
    SQL.Strings = (
      'SELECT DISTINCT MEMBERID, PERIODID, DESCRIPTION'
      'FROM PREFERENCE '
      'INNER JOIN PERIODS ON PERIODS.ID = PREFERENCE.PERIODID'
      'WHERE MEMBERID = :pMEMBERID'
      'ORDER BY PERIODID')
    Left = 144
    Top = 16
  end
  object dsMEMPREF: TDataSource
    DataSet = QMEMPREF
    Left = 144
    Top = 64
  end
  object QUPD1: TADOQuery
    Connection = SyndicateDB
    Parameters = <>
    Left = 432
    Top = 32
  end
  object QUPD2: TADOQuery
    Connection = SyndicateDB
    Parameters = <>
    Left = 432
    Top = 80
  end
  object Qbadfirst: TADOQuery
    Connection = SyndicateDB
    Parameters = <
      item
        Name = 'pweekno'
        Attributes = [paNullable]
        DataType = ftFixedChar
        NumericScale = 255
        Precision = 255
        Size = 510
        Value = Null
      end>
    SQL.Strings = (
      
        'select max(prefno) as high, min(prefno)  as low from preference ' +
        'where weekno = :pweekno')
    Left = 264
    Top = 104
  end
  object Qtemp: TADOQuery
    Connection = SyndicateDB
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select * from temp')
    Left = 448
    Top = 232
  end
  object QDelTemp: TADOQuery
    Connection = SyndicateDB
    Parameters = <>
    SQL.Strings = (
      'delete from temp')
    Left = 488
    Top = 104
  end
  object QITemp: TADOQuery
    Connection = SyndicateDB
    Parameters = <
      item
        Name = 'pmemno'
        Attributes = [paNullable]
        DataType = ftFixedChar
        NumericScale = 255
        Precision = 255
        Size = 510
        Value = Null
      end
      item
        Name = 'pmember'
        Attributes = [paNullable]
        DataType = ftFixedChar
        NumericScale = 255
        Precision = 255
        Size = 510
        Value = Null
      end
      item
        Name = 'pweek'
        Attributes = [paNullable]
        DataType = ftFixedChar
        NumericScale = 255
        Precision = 255
        Size = 510
        Value = Null
      end
      item
        Name = 'ppref'
        Attributes = [paNullable]
        DataType = ftFixedChar
        NumericScale = 255
        Precision = 255
        Size = 510
        Value = Null
      end
      item
        Name = 'pweekdes'
        Attributes = [paNullable]
        DataType = ftFixedChar
        NumericScale = 255
        Precision = 255
        Size = 510
        Value = Null
      end>
    SQL.Strings = (
      'insert into temp (memno,member,week,pref,weekdes) '
      'values (:pmemno,:pmember,:pweek,:ppref,:pweekdes)')
    Left = 488
    Top = 152
  end
  object Qperiod: TADOQuery
    Connection = SyndicateDB
    Parameters = <
      item
        Name = 'pid'
        Attributes = [paNullable]
        DataType = ftFixedChar
        NumericScale = 255
        Precision = 255
        Size = 510
        Value = Null
      end>
    SQL.Strings = (
      'select * from periods where :pid between beginweek and endweek ')
    Left = 184
    Top = 232
  end
  object QMU: TADOQuery
    Connection = SyndicateDB
    Parameters = <>
    Left = 448
    Top = 304
  end
  object QRepMember: TADOQuery
    Connection = SyndicateDB
    Parameters = <>
    SQL.Strings = (
      'select * from member order by id')
    Left = 312
    Top = 16
  end
  object tweek: TADOTable
    Active = True
    Connection = SyndicateDB
    CursorType = ctStatic
    TableName = 'Week'
    Left = 24
    Top = 192
  end
  object QStore: TADOQuery
    Connection = SyndicateDB
    Parameters = <
      item
        Name = 'allocated2'
        Attributes = [paNullable]
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 510
      end
      item
        Name = 'allocated1'
        Attributes = [paNullable]
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 510
      end>
    SQL.Strings = (
      'update preference set allocated1 = allocated2')
    Left = 240
    Top = 208
  end
end
