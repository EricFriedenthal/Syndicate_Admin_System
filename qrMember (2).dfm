 
 TREPMEMBER 0Y╩  TPF0
TrepMember	repMemberLeft Top WidthHeightcFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRight
AfterPrintQuickRepAfterPrintBeforePrintQuickRepBeforePrintDataSetDataModule1.QRepMemberFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Heightє	Font.NameArial
Font.Style Functions.Strings
PAGENUMBERCOLUMNNUMBERREPORTTITLE Functions.DATA00'' OptionsFirstPageHeaderLastPageFooter Page.ColumnsPage.Orientation
poPortraitPage.PaperSizeA4Page.Values       ╚@      а╣
@       ╚@      @Г
@       ╚@       ╚@           PrinterSettings.CopiesPrinterSettings.DuplexPrinterSettings.FirstPage PrinterSettings.LastPage PrinterSettings.OutputBinAutoPrintIfEmpty	
SnapToGrid	UnitsMMZoomd TQRBandQRBand1Left&Top&Width╬Height[Frame.ColorclBlackFrame.DrawTop	Frame.DrawBottom	Frame.DrawLeft	Frame.DrawRight	AlignToBottomColorclWhiteForceNewColumnForceNewPageSize.ValuesTUUUUU┼Ё@ ░ккккvэ	@ BandTyperbTitle 
TQRSysData
QRSysData1Left@TopDWidthЗ HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Valuesккккккъ│@      А╛	@ккккккъ│@      Ш▓@ 	AlignmenttaRightJustifyAlignToBandAutoSize	ColorclWhiteDataqrsDateTimeTextPrinted on :TransparentFontSize
  TQRLabel Left Top>WidthvHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesккккккJД@ XUUUUUй@кккккк
д@ккккккЬ@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchCaptionMember ListColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Heightь	Font.NameArial
Font.StylefsBold 
ParentFontTransparentWordWrap	FontSize   TQRBandQRBand2Left&TopБ Width╬HeightFrame.ColorclBlackFrame.DrawTop	Frame.DrawBottom	Frame.DrawLeft	Frame.DrawRight	AlignToBottomColorclWhiteForceNewColumnForceNewPageSize.ValuesUUUUUU╔@ ░ккккvэ	@ BandTyperbColumnHeader TQRLabelQRLabel1LeftTopWidthHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Valuesккккккъ│@ XUUUUUй@ XUUUUUй @      А╛@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchCaptionNoColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Heightє	Font.NameArial
Font.StylefsBold 
ParentFontTransparentWordWrap	FontSize
  TQRLabelQRLabel2Leftр TopWidthaHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Valuesккккккъ│@кккккк*Ф@ XUUUUUй @ккккккRА@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchCaptionContact PersonColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Heightє	Font.NameArial
Font.StylefsBold 
ParentFontTransparentWordWrap	FontSize
  TQRLabelQRLabel3Left@TopWidthHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Valuesккккккъ│@      А╛	@ XUUUUUй @     @▐@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchCaptionTelColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Heightє	Font.NameArial
Font.StylefsBold 
ParentFontTransparentWordWrap	FontSize
  TQRLabelQRLabel4Left(TopWidth$HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Valuesккккккъ│@ккккккк╙@ XUUUUUй @      А╛@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchCaptionEntityColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Heightє	Font.NameArial
Font.StylefsBold 
ParentFontTransparentWordWrap	FontSize
   TQRBandQRBand3Left&TopФ Width╬HeightFrame.ColorclBlackFrame.DrawTop	Frame.DrawBottom	Frame.DrawLeft	Frame.DrawRight	AlignToBottomColorclWhiteForceNewColumnForceNewPageSize.Values XUUUUUй@ ░ккккvэ	@ BandTyperbDetail 	TQRDBText	QRDBText1LeftTop Width
HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values      └Ю@ XUUUUUй@          ккккккк╙@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchColorclWhiteDataSetDataModule1.QRepMember	DataFieldIDFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Heightї	Font.NameArial
Font.Style 
ParentFontTransparentWordWrap	FontSize  	TQRDBText	QRDBText2Leftс Top WidthHHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values      └Ю@      ╘Ф@                А╛@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchColorclWhiteDataSetDataModule1.QRepMember	DataFieldContactPersonFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Heightї	Font.NameArial
Font.Style 
ParentFontTransparentWordWrap	FontSize  	TQRDBText	QRDBText3Left@Top WidthHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values      └Ю@      А╛	@                └Ю@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchColorclWhiteDataSetDataModule1.QRepMember	DataFieldTelFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Heightї	Font.NameArial
Font.Style 
ParentFontTransparentWordWrap	FontSize  	TQRDBText	QRDBText4Left)Top Width6HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values      └Ю@UUUUUUї╪@                рО@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchColorclWhiteDataSetDataModule1.QRepMember	DataField
EntityNameFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Heightї	Font.NameArial
Font.Style 
ParentFontTransparentWordWrap	FontSize   TQRImageQRImage1Left(Top)WidthBHeight:Frame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUUuЩ@ккккккк╙@UUUUUUї╪@TUUUUU¤╘@ Picture.Data
2▓  TBitmap&▓  BM&▓      6  (   т  \         Ён                       !!! ))) 111 999 BBB JJJ RRR ZZZ ccc kkk sss {{{ ДДД МММ ФФФ ЬЬЬ еее ннн ╡╡╡ ╜╜╜ ╞╞╞ ╬╬╬ ╓╓╓ ▐▐▐ ўўў ччя яяў ╞╞╬ ╓╓▐ ▐▐ч ╡╡╜ еен нн╡ ╜╜╞ ММФ ФФЬ ЬЬе ДДМ ss{ kks {{Д cck RRZ ZZc JJR BBJ 99B 119 ))1 !!) ╡╜╞ ФЬе s{Д RZc чяў яў  ╬╓▐ н╡╜ ен╡ МФЬ ДМФ ks{ cks ╜╞╬ Ьен {ДМ Zck н╜╞ МЬе k{Д ╞╓▐ е╡╜ ДФЬ ╓чя ╜╬╓ ╡╞╬ Ьн╡ Фен {МФ sДМ ╞▐ч е╜╞ ╓яў ╡╬╓ н╞╬ Ь╡╜ Фн╡ ╜▐ч ╞чя ╡╓▐ н╬╓ е╞╬ ╞яў ╜чя чяя яўў ў   ╞╬╬ ╬╓╓ ╓▐▐ ▐чч ╡╜╜ енн н╡╡ ╜╞╞ МФФ ФЬЬ Ьее ДММ s{{ чўў я   kss {ДД ╬▐▐ ▐яя ckk ╞╓╓ н╜╜ ╡╞╞ RZZ е╡╡ Zcc JRR ч   Ьнн МЬЬ Фее ╓яя BJJ ДФФ ╜╓╓ ╡╬╬ е╜╜ н╞╞ k{{ ▐   9BB ╓ўў css Ь╡╡ Фнн Мее ДЬЬ ╡╓╓ н╬╬ ╓   е╞╞ ╬ўў ╞яя Ь╜╜ Ф╡╡ ╜чч ╡▐▐ Мнн н╓╓ ╬   е╬╬ ╜╡╜ нен ╡н╡ ╞╜╞ ФМФ ЬФЬ еЬе МДМ {s{ sks Д{Д kck ZRZ cZc RJR 1)1                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  	                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               

                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               		                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               	
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               	                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              	                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            2                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              `аg5                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              ааЦ                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             аааа                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             аааа                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             аааа
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             аааr
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             ааа
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             ааа	                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             ааа                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            ааа                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        ааа                                                                                                                                                                                                     		                                                                                                                                                                                                                                      ]ааа5                                                                                            
                            	                                                          

     	                  
			
                      

               	
                                                                                           
			
                       		                         
	Яаааu                                                                                    	                          
                                                
               

                  	           
	                                    



                                    
	                  		                1бааа)            
			
                                                                 
                        
	        
	                   
	           

	              

                 
        

                        
										






												
         

                	            
		Таааu          

                                                               	                       

      

      	        
                    		              
      	
                     
	       		              	
          	ГаааЛ
                                                                       	                      	


           
             
          	

             

     	
                    
	




		     
	
	            
        
Hааа8	        	                                                                                 	     	
	

       
	                        		       
           
	     	
                   
		
   
										      
       
          		

	
      ▓DаааС	        
▒                                                               	                      .
        
			       	       	                     
о                     	   	         	
                  
				

	                       
			
      	x          	         	   
     
▓   CаааРн      ▓Е     	                                                          	                      
          
	     	▓           ▓          
       	            
          	      	            
                                                                                  
         
       	     3
       	аааЬ*      	       
	                                                          4
                     
0,          
     
     	              ▓34IбааЭЯ$         
                      +B        5+             
	                             	                                                            ~-*)        	     0.~+q        	ааа`      	▓                                                                  аЗ)	                     Е9У_ааа         	
       
▓▓
       ▓       aаЭ      /Bааааааааw        *,  aаааа`Ш   		        ~и,HЭаа        39Юбааа аЦ;       	                            	5	                                       +q?  ааа`аШM 0
       
.S_ааа         
     //УЭааа        Цаа`▓▓
     ▓4542.                                                                 аааК
                    -B_ааааааааааА     н▓▓441-         ▓▓▓	       ▓
       ааааa    Lааааааааааd  
    5+q_аааааааааааа  -
       EЭааа`ааa     249ЯааааааааааЦd      
                            *Н'q8)                                       q8_ааааааааааааX)л▓     /FЭааааааа`Мs     	    32Е	+Lааааааааа`     Цааа▓	     |0FLЪааааЯ          
                                                       аааа/	                    
,бааааааааааааааs    /ПЫааа         
'+
▓▓      ,+	       аааааа   (Dаааааааааааа  
   о-YааааааааааааааааЮ.0	     ,Ьаааааааа`b   о▓ЕЭааааааааааааааА     

                            аааааааа   ааааааааааЦЦЦЦЦЦМААss;             ',YааааааааааааааааЫ0▓
.Bааааааааааааа;    2.9баа_Eкu^ааааааааааааЦ   Мааа▓    /Baааааааааааа        
                                                      аааа
                    	{ааааа     аааааааs    	57aаааааааа       `аЭB▓
     аа+     аааааааЭqаааааааааааааw 	л	/tааааааа`ааааааааааааПЕpPааааааааааа;   )@аааааааааааааааааМ     	
             sЦаааааааааааааааааааааааааааааааааааааааааааааааааааа     7ааааааааааааааааааааB1


0BаааааааааааааааА    
ПааааааЮUааааааааааааааа  АаааM▓    Уаааааааааааааа        	                                                      аааС/
                    <аааааа      sЦаа`аа;   yааааааааааа`d   rаааа0▓	)ааааЯ
   ааааааааy	aааааа й  ааааVЕПаааааааШМr
mРaаааааY/+аааааааааааааА  РаааааааааааааааааааМ    

            Цаааааааааааааааааааа      аааааааааааааааааааааааааааа     ░6аааааааЩMr#КaаааааЮ|2ааааааа   аааааааА   -ааааааааааааа      аааааЦ sаааМ▓
     аааааааааааааааа       	                                                      ааа
                   `а`ааа         Ааааааd  +ааааааааааааааd  АааааA▓▓Rааааа*	&ааааФ)Фааа`lаааааp*    аааа	1Fаааааа    /
8ЭааааX	.УааааЭ     аааа yааааааа       dАаааааМ    
            ЦаааааааааМА   аааЩ'              dddddssАsАММааааааааа    	yаааааа    пSЭаааа_5Юаааааа    ДМааааааs   &Ыааааааааааааа      ааааааМ;аааМ▓	     аааааЬPu-)}Яаааа      
                                                     ааа                   wааааа           dааааа  Цаааааааааааааа`s Маааа?▓▓HаааааHйDааааq'ааааaааааZx    Vааа5Е		Fаааааа     

   ааааЗ	Еааааа~     ааааажН`аааааа           АааааА                dАМаааАd      аааЙж                            dsАМаа     wаааааа     

x+YааааФ
3ааааа^       dМааааа;  nаааааааааааааР       ОаааааМаааа     ааааR    	(Рааа                                                          ааа,                   аааааЪ            dааааЦАааааааЖ*&   аааа;ЦааааЮ▓	ЫаааааЭ+
ЧаааЬ'Фаааааааa

     аааz1ааааа      
     ааа`<н	AааааFп     ОааааАкАаааааЭ             sаааа;                              ааа'	                                       аа`аа      
    ааа`<УааааЯ         sааааа   
yаааааааaаааа[        sааааааааа▓    аааааF     
))Ьаа     	                                                     ааа)                   ааааa*             dаааааааааа+/     аааааааааЯ2_аааааа		,ЯаааЖ.#аааааааE.
     аааW+▓3Уаааа]      

      Шааааq04баааЭ2      ЦааааuаааааЭм              sаааМ                              ааа
                                      Маааа       

+      Щаааа*
4ааааа/          sааааМ  2`аааа   ааааа         Аааааааааr   аааааа      
,Ьаа                                                          аааа)                   `аааВ              АааааааааС	     аааааааааЩ-3аааааааYAаааа'ЬаааааЩ     ШааЬ▓2`аааЬ-            аааа
2ааааF      ;ааааА`аааЩ,               Мааа                               аааи                                      ааааЩ      Н       ааааc
4FааааВ           Аааааs gаааа    ааааT          ЦаааааааА▓/бааааааааа     
aа                                                            аааа                   аааЩ+               МааааааЩ,      ааааааааа+аааааааЭ
аааа5
   аааааY3     Оааа-Юаааа|/     u+uZЯааааааааааШ	   xаааа.         аааааааp                ааа                               аааo                                      ааааm      )ЫЯааааааааааЦ	    ааааF            АаааЦ#hааа     аааа          dаааааааЦ▓.ааааааааааа     	)nаа                                                           аааа&	                  аааа}               dаааааа[      dааааааааHаааааааа@)аааЧ5   ааааа*     ШаааааааL0	     3(Kaаааааааааааааа[)-     аааб	         ааааааЭп
                sаа                               ааа#
                                      ааа[й	     ^aааааааааааааааШ     Эаааaп
             Цааа;МаааQ   ЬаааЩ4
           Цааааааа▓▓
аааааааааааа     6а                                                           ааааз	                  ааааo	                АаааааC       ЦаааааааЭ	9аааПааааK|pаааS   Ьааа`	      ааT)░Ваааа       аааааааааааааааааааУ      аааR         аааааq
                 аа                               ааШ	                                      ааа&     jЗаааааааааааааааааааY    ЭаааW	             dаааааааК+   ааааT	           sааааааа▓▓0аааа/xЭаааааа    &aа                                                          ааааo	                  аааШ#               ааааа
       :аааааааа	RаааЕLаааaгQааа9   Wаааy     ааi
^аааJ
      ааааааааааааааааааааа_    _аааB         ааааa0                Ма                               ааШ                                    аааО
     ^аааааааааааааааааааааЭ   ааааl             МаааМаааW   ааааe           аааааааg0аааа 	пHаааааа    	(а                                                          аааа	                   аааД               Ааааа	       ДааааааааXYааа	аааа{
Gааа2   Ьааа^к    ааj#дi`аааC    ааааааU        Д`ааааааб   ааааН        ааааE
                sа                               ааM                                    аааw    ааааааU        MШааааааЭ nааааe             dаааааааБ   ааааC           Аааааааf
Йааа  Эааааа    	[                                                          аааа5	                   аааM
               dа`аа       ааааааааа&	Яааа0/ХаааaФаааН	   Tаааa   аааa[pиааааf	  аааааS	.          аааааааб  ааааН	



	,аааа2▓	                 аа                              аа\▒                                    аааН	  аааааЖ
/          аааааааЭ(аааа3
               ааааааD   аааа|            ааааааy#
Yааа  
зzааааа   
	o                                                         ааааа$	                   аааb3               sааааf	       аа`ааааааX+ааааГаааа!aааа   `аааа,▓  а`ааа`Ш"аааа
	-_ааа]	
         аааааааа  аааа0▓.аааа▓                Ца                              ааg                                    ааа
	+_ааа].1
         ааааааааЯаааа	                ааааа7   aааа
             аааа[&Hааа   	4kаааа                                                              Цаааа                 аааа		                 аааД      ЦаааОTаааакEаааШ
(ааааО`ааа2	    аааа▓	+ЪаааааааааааааBаааЭ0	     *аааЭааааLаааа▓▓ЕааааB▓                АЦ                              ааf                                    ааа8аааЭ
	н     Aааа+Эаааааааа
               ааааа    ааа
            аааааВ	Eааа    зРаааа   	                                                           Маааа5                 аааа                ааа`	     ЦааШeааааGPаааaJаааа(аааа.	     ааа`Aаааааааааааааа+QаааB

ааа-[аааааааа3▓▓▓▓	ааааL                sА                              аа                                   ааааu,РаааA

ааа*   ааа`аа              ааааа-   ааа	            ааааааF   а    
{аааа 	                                                           sаааа5                 Маа`	               аа`аK-▓	      Мааа4ааааaФааа`ааааМаааа-	     аааЗ&2Таа`    ааааааа  
Ьаааааа    ааааааа1▓▓▓▓/]аааЧ-	                                                ааЕ4                                   ааааs 
'aааа▓ааа     ааааа:
	              ааааа   аааC

           ааааааЮ.     аa    
'[аааZ-	                                                           sаааа5
                 Маааd               ааааЬ▓
      Цааа\ааааЗaаааДОаааа#аааа
     ааа42Эааа    а`аааааА  (аааа.8ааа  /  аааааа-▓▓[аааa)		
                                                  ааe                                   ЦаааА  аааа/*аа      аааааc
             Tаааа2   ааа   		        аааааааа     аа!    
'аааа	                                                           sаааа	                 sаааА  	            ааааа
      %Цааа`ааааааааАcааааАа`аа     ааа<ааа      Цаааааа    
аааа   аа      `ааааа)	



Заааа                                                   ааC                                   Цаааа    
аааа,  аа      ааааа   
           Zаааа3   ааа          tаааааааа      ааО     
Uааа@
                                                           аааа	                 dаааЦ   

          ааааа0
       МаааД`ааа`ааа; АааааАаааа.      ааа`6  	ааа       Мааа`аА      ааа  								
 aаа       ааааа{Н      а`ааЗ$                                                   ааФм                                   sааааА      аааЭ 						
 Эа       bаааа   
	       	+аааа4    аа    

	uаааааа       аааd     
аааЭ-                                                           аааа	                  Цааа    	
       баааааЕ
         Мааа#Цаааааааd   dаааааааа         аааааQ     .aаа        Аааааа      аааа              ааа       sаааа`j5	      Ааааа`n                                                   аа[                                   dааааа      аааа              аа        dааа     	
	░Ьааа3   ааа    
tаааа        аааЦ     
Tаааu                                                           еiаааа5	                  АаааА   		Тааааа.
          АаааcвАааааааа       Мааааааа         аааааа      8ааФ        ЦааааМ      а`а`            `ааа`ОЦr;  ааааааC&B0|BI    ``аааааав                                                   ааT                                    ЦааааМ     Маааа            `аа         Цаа     
Сааа    аа      	/*ааа        Ааааs      
fаааO                                                             аааа	                  dаааа    Aаааааu*           АаааdЦаааааЦ       dааааааа        Цааааааа     м       АаааааЦ     ЦааааM         Шаааааааааааааааааааааааааааааааааааа
                                                  аа`
                                    sаааааЦ     Цаа`аw         Шааа         МаааЦ   Eааа	   ааа      	_ааа         ЦааЦ        ааа                                                              iаааа	                   АаааМ    	ааааааa           АаааА   dаааааМ        Ааааааа        Маааааааa    	        МааааааЦs   аааааа       Мааааааааааааааааааааааааааааааааааааа6'                                                 ааа-
                                     МааааааЦs   аааааа       Маааа         dааааs   ░*ааа+   ааа        
					░Gаааа         dаааа       ааа                                                              аааа	                   dаааа;     	[аааааа           sаааЦ    Аааааd        dаааааа        sааааааааЫ           dааааааааааааааааааs   АЦааааАЦааааааааааааааааааааааааааааааааЩ                                                 ааU
                                     dааааааааааааааааааs   АЦааааА          Цаааа    *,pааа    ааа                ааааа          sаааЦ      ааа                                                              %ааааC	                    Ааааа       
					,Ваааааа           Ааааа     МаАd          Ааааааа       sааааhаааа            dааааааааааааааааааааааааааЦ sааааааааааааааааааааааааа`аааааа`                                                 ааК
                                      dааааааааааааааааааааааааааЦ           sааааА     
				
^аа`аа6     ааа               ааааО           МаааЦs Аааа                                                              ааааe                    ааааа;                  аааааа           dаааЦ                    аааааЦ       sааааfаааЩ    	         dЦаааааааааааааааааааааааЦ   dsМЦаАЦаааааааааааааа        ЦаОf                                                 аа                                       dЦаааааааааааааааааааааааЦ             аааааs       #^ааааааf	    ааааМ           аааааа             ЦааааАd Ааааа                                                          ааааC                    dаааааА                 аааааа            АаМd                    sааааd        ааааwTаааY   	          dМааааааааааааааааааааА            sааааааЗ,*)          
	
                                                  аа'                                         dМааааааааааааааааааааА              dаааааА           [аааааааV     ааааd       Аааааааd              ЦаааааааааааЦ                                                           ааааC                     sаааааЦd              ааааааА                                     sаЦd         Цаааrаааа               dsМЦааааааааааааЦМs               аааааа	                                                                 аа^+                                           dsМЦааааааааааааЦМs                 МаааааМ          Ш`аааааааа      ааааМd  sМаааааааs                ЦааааааааааА                                                              ааааK                         аааа           ;аааааааЦ                                                   МаааА0ЗаааX   
                                              Цаааааg	                                                                  аа`                                                                                аааааааМs   sАаааааааааааа      ааааааааааааааааd                  АааааааааМ                                                               ааааГ                          ааааа       АааааааааЦ                                                    АаааАдаааа   	                                              АаааааV
                                                                  ааа                                                                                dаааааааааааааааааааМАааааf
      MаааааааааааааМd                    dАЦаЦАsd                             
										
                      аааа?	                            ааааааааааааааааааМ                                                     dааааФаааЪ                                               dаааааО                                                                 аааа+	                                                                                 dЦааааааааааааааааЦ dааааО	       bЦаааааааааЦs                                                          		
               ааааЮ                              ааааааааааааааЦs                                                       аааа	#аааа                                                аааааап        Маааа                                                  аааааа-	                                                                                   МааааааааааааааА  dаааа`         dАЦаааЦАs                                                            				
       ааааР                                 ааааааааааА                                                         ЦаааsWааа  
                                              Цаааа           ааааааа                                                аааааа-	                                                                                     sЦаааааааааЦАd   sааааа                                                                              ▓				 ааааР               

		           аааЦАd                                                           Маааs	jаааЭ                                                Ааааа           ааааааа                                                Цааааа                                                                                       sМЦааЦМsd      аааааf                                                                               ааааK









		                                                                         sаааМ
5Щааа                                               dаааа           sаааааА                                                Аааааа)	                                                                                                       аааааД
                                                                              
 аааа   

		                                                                        ааа`&н{ааа                                                Цааа            АаЦАs                                                 dаааааm░                                                                                                       ааааа`	                                                                                   
																		░
 аааа          









								
                                                                        Маааs	jаааЯ*                                              Мааа                                                                  ааааа                                                                                                       Цааааа	                                                                                                                  аааа                                                                                                           Ааааsaааа*                                              sаааа                                                                 ааааа
                                                                                                        Цааааа 	                                                                                                                 аааа                                                                                                           dаааЦ	Tаааp	                                               аааа                                                                 "ааааа/                                                                                                         Аааааа\
                                                                          АаааааЦАМААМААs                        dаааа                                                                                                            Цааа  
eаааZ0	                                                Цааа                                                                 lаааа                                                                                                           sаааааО                                                                           ааааааааааааааааааааЦА                 sаааа                                                                                                            Ааааs  5аааЭ.	                                                sааd                                                                 аааа                                                                                                           dаааааа
                                                                            ааааааааааааааааааааааАssssssd        dЦааааа                                                                                                           dаааМ  ``аа
                                                                                                                      sаааа                                                                                                            аааааа                                                                              Маааааааааааааааааааааааааааааааааааааааааааа                     dsssssssМаs                                                                            Цааа   
Дааа@3                                                                                                                      dаааа                                                                                                            Цааааа                                                                               Мааааааааааааааааааааааааааааааааааааааааааааааааааааааааааааааааааааааааааа                                                                            sаааА   hаааКм
                                                                                                                       sаааа                                                                                                            Аааааа                                                                                АЦааааааааааааааааааааааааааааааааааааааааааааааааааааааааааааааааааааааааЦ                                                                             аааЦ    ааа\                                                                                                                        Цааааа                                                                                                           Аааааа                                                                                     sАМММММааааааааааааЦМММsАssddd                 dsАМЦааЦsdаааааааЦММММd                                                                             Маааd   ааа                                                                                                                          аааааа                                                                                                           sааааа                                                                                                                                                                                                                                        dаааЦ   ааа                                                                                                                          аааааs                                                                                                            аааааа                                                                                                                                                                                                                                        Цааа;  ааа                                                                                                                          sаМd                                                                                                              Мааааа                                                                                                                                                                                                                                        sаааЦ  ааа                                                                                                                                                                                                                                            sааааЦ                                                                                                                                                                                                                                         ааааАМааа                                                                                                                                                                                                                                             sааМ                                                                                                                                                                                                                                          sааааааааМ                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           Ааааааааs                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            МаааааЦ                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              sЦааМd                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     Stretch	   