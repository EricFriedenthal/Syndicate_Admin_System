unit Periods;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ComCtrls, Grids, DBGrids, ExtCtrls, DBCtrls;

type
  TForm3 = class(TForm)
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    DBGrid1: TDBGrid;
    TabSheet2: TTabSheet;
    DBGrid2: TDBGrid;
    DBNavigator1: TDBNavigator;
    Label3: TLabel;
    Label1: TLabel;
    MonthCalendar1: TMonthCalendar;
    Button1: TButton;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure Button1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
   No : Integer;
    { Public declarations }
  end;

var
  Form3: TForm3;

implementation

uses Main, data;

{$R *.DFM}

procedure TForm3.FormClose(Sender: TObject; var Action: TCloseAction);
var i1 : Integer;
begin
 if data.DataModule1.tPeriod.Modified then
  data.DataModule1.tPeriod.Post;
end;

procedure TForm3.Button1Click(Sender: TObject);
var vDate : TDateTime;
begin
  data.DataModule1.QUPD2.SQL.Text := 'update week set startdate = :pstartdate where weekno = :pweekno';
  datamodule1.tweek.First;
  vDate := MonthCalendar1.Date;
  while not datamodule1.tweek.Eof do
  begin
    data.DataModule1.QUPD2.Parameters.ParamByName('pstartdate').value := vDate;
    data.DataModule1.QUPD2.Parameters.ParamByName('pweekno').value := datamodule1.tweek['weekno'];
    data.DataModule1.QUPD2.ExecSQL;
//    showmessage('busy');
    vDate := vDate + 7;
    datamodule1.tweek.Next;
  end;
  datamodule1.tweek.Requery;
  Showmessage('Weeks table updated sucessfully');
end;

procedure TForm3.FormShow(Sender: TObject);
begin
 // MonthCalendar1.Date := now;
end;

end.
