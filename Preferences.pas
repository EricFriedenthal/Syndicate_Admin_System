unit Preferences;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DBCtrls, Grids, DBGrids, StdCtrls, ExtCtrls;

type
  TForm4 = class(TForm)
    DBLookupComboBox1: TDBLookupComboBox;
    DBGrid1: TDBGrid;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    DBLookupComboBox2: TDBLookupComboBox;
    Button1: TButton;
    procedure DBGrid1CellClick(Column: TColumn);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure DBLookupComboBox1CloseUp(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure Button1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form4: TForm4;

implementation

uses data;

{$R *.DFM}

procedure TForm4.DBGrid1CellClick(Column: TColumn);
begin
//  data.DataModule1.tPreference.Requery;
end;

procedure TForm4.FormCreate(Sender: TObject);
begin
  dblookupcombobox1.KeyValue := data.DataModule1.tMember['id'];
end;

procedure TForm4.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  if data.DataModule1.tPreference.Modified then
    data.DataModule1.tPreference.Post;
end;

procedure TForm4.DBLookupComboBox1CloseUp(Sender: TObject);
begin
  Data.DataModule1.QMEMPREF.Close;
  Data.DataModule1.QMEMPREF.Parameters.ParamByName('pMEMBERID').Value := Data.DataModule1.tMember['ID'];
  Data.DataModule1.QMEMPREF.Open;
  Data.DataModule1.tPreference.Close;
  Data.DataModule1.tPreference.Open;
end;

procedure TForm4.FormShow(Sender: TObject);
begin  
  Data.DataModule1.QMEMPREF.Close;
  Data.DataModule1.QMEMPREF.Parameters.ParamByName('pMEMBERID').Value := Data.DataModule1.tMember['ID'];
  Data.DataModule1.QMEMPREF.Open;  
  Data.DataModule1.tPreference.Close;
  Data.DataModule1.tPreference.Open;
  DBLookupComboBox2.KeyValue := 1;
end;

procedure TForm4.Button1Click(Sender: TObject);
var ttot : integer;
begin
  ttot := 0;
  Data.DataModule1.tPreference.First;
  while not Data.DataModule1.tPreference.Eof do
  begin
    ttot := ttot +Data.DataModule1.tPreference['PREFNO'];
    Data.DataModule1.tPreference.Next;
  end;
  if ttot <> 91 then
  begin
    showmessage('Not all values are correct, please check');
    ActiveControl := DBGrid1;
  end;
end;

end.
