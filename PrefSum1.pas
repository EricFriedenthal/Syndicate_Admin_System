unit PrefSum1;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  QuickRpt, Qrctrls, ExtCtrls;

type
  TRPrefSum1 = class(TForm)
    QRPrefSum1: TQuickRep;
    QRBand1: TQRBand;
    QRSysData1: TQRSysData;
    QRLabel17: TQRLabel;
    QRShape33: TQRShape;
    QRShape38: TQRShape;
    QRShape39: TQRShape;
    QRBand2: TQRBand;
    QRShape36: TQRShape;
    QRShape32: TQRShape;
    QRShape29: TQRShape;
    QRLabel1: TQRLabel;
    QRLabel2: TQRLabel;
    QRLabel3: TQRLabel;
    QRLabel4: TQRLabel;
    QRLabel5: TQRLabel;
    QRLabel6: TQRLabel;
    QRLabel7: TQRLabel;
    QRLabel8: TQRLabel;
    QRLabel9: TQRLabel;
    QRLabel10: TQRLabel;
    QRLabel11: TQRLabel;
    QRLabel12: TQRLabel;
    QRLabel13: TQRLabel;
    QRShape1: TQRShape;
    QRShape2: TQRShape;
    QRShape3: TQRShape;
    QRShape4: TQRShape;
    QRShape5: TQRShape;
    QRShape6: TQRShape;
    QRShape7: TQRShape;
    QRShape8: TQRShape;
    QRShape9: TQRShape;
    QRShape10: TQRShape;
    QRShape11: TQRShape;
    QRShape12: TQRShape;
    QRShape13: TQRShape;
    QRShape14: TQRShape;
    QRLabel15: TQRLabel;
    QRShape30: TQRShape;
    QRShape31: TQRShape;
    QRLabel16: TQRLabel;
    QRShape35: TQRShape;
    QRBand3: TQRBand;
    QRShape15: TQRShape;
    QRShape16: TQRShape;
    QRShape17: TQRShape;
    QRShape18: TQRShape;
    QRShape19: TQRShape;
    QRShape20: TQRShape;
    QRShape21: TQRShape;
    QRShape22: TQRShape;
    QRShape23: TQRShape;
    QRShape24: TQRShape;
    QRShape25: TQRShape;
    QRShape26: TQRShape;
    QRShape27: TQRShape;
    QRShape28: TQRShape;
    W1: TQRLabel;
    W2: TQRLabel;
    W3: TQRLabel;
    W4: TQRLabel;
    W5: TQRLabel;
    W6: TQRLabel;
    W7: TQRLabel;
    W8: TQRLabel;
    W9: TQRLabel;
    W10: TQRLabel;
    W11: TQRLabel;
    W12: TQRLabel;
    W13: TQRLabel;
    QRDBText1: TQRDBText;
    QRShape34: TQRShape;
    QRShape37: TQRShape;
    QRGroup1: TQRGroup;
    QRBand4: TQRBand;
    QRImage1: TQRImage;
    procedure QRBand1BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRBand3BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRBand4BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QRBand3AfterPrint(Sender: TQRCustomBand;
      BandPrinted: Boolean);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  RPrefSum1: TRPrefSum1;
  vP1, vP2, vP3, vP4, vP5, vP6, vP7, vP8, vP9, vP10, vP11, vP12, vP13, vPP : Integer;

implementation

uses data;

{$R *.DFM}

procedure TRPrefSum1.QRBand1BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  vP1 := 0;
  vP2 := 0;
  vP3 := 0;
  vP4 := 0;
  vP5 := 0;
  vP6 := 0;
  vP7 := 0;
  vP8 := 0;
  vP9 := 0;
  vP10 := 0;
  vP11 := 0;
  vP12 := 0;
  vP13 := 0;

end;

procedure TRPrefSum1.QRBand3BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  W1.Caption := inttostr(vP1);
  W2.Caption := inttostr(vP2);
  W3.Caption := inttostr(vP3);
  W4.Caption := inttostr(vP4);
  W5.Caption := inttostr(vP5);
  W6.Caption := inttostr(vP6);
  W7.Caption := inttostr(vP7);
  W8.Caption := inttostr(vP8);
  W9.Caption := inttostr(vP9);
  W10.Caption := inttostr(vP10);
  W11.Caption := inttostr(vP11);
  W12.Caption := inttostr(vP12);
  W13.Caption := inttostr(vP13);

end;

procedure TRPrefSum1.QRBand4BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  if Data.DataModule1.QReport['PREFNO'] = 1 then
    inc(vP1);
  if Data.DataModule1.QReport['PREFNO'] = 2 then
    inc(vP2);
  if Data.DataModule1.QReport['PREFNO'] = 3 then
    inc(vP3);
  if Data.DataModule1.QReport['PREFNO'] = 4 then
    inc(vP4);
  if Data.DataModule1.QReport['PREFNO'] = 5 then
    inc(vP5);
  if Data.DataModule1.QReport['PREFNO'] = 6 then
    inc(vP6);
  if Data.DataModule1.QReport['PREFNO'] = 7 then
    inc(vP7);
  if Data.DataModule1.QReport['PREFNO'] = 8 then
    inc(vP8);
  if Data.DataModule1.QReport['PREFNO'] = 9 then
    inc(vP9);
  if Data.DataModule1.QReport['PREFNO'] = 10 then
    inc(vP10);
  if Data.DataModule1.QReport['PREFNO'] = 11 then
    inc(vP11);
  if Data.DataModule1.QReport['PREFNO'] = 12 then
    inc(vP12);
  if Data.DataModule1.QReport['PREFNO'] = 13 then
    inc(vP13);

end;

procedure TRPrefSum1.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action := caFree;

end;

procedure TRPrefSum1.QRBand3AfterPrint(Sender: TQRCustomBand;
  BandPrinted: Boolean);
begin
  vP1 := 0;
  vP2 := 0;
  vP3 := 0;
  vP4 := 0;
  vP5 := 0;
  vP6 := 0;
  vP7 := 0;
  vP8 := 0;
  vP9 := 0;
  vP10 := 0;
  vP11 := 0;
  vP12 := 0;
  vP13 := 0;

end;

end.
