object Form3: TForm3
  Left = 178
  Top = 136
  Width = 813
  Height = 481
  Caption = 'frmPeriods'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -14
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 120
  TextHeight = 16
  object PageControl1: TPageControl
    Left = 0
    Top = 0
    Width = 805
    Height = 448
    ActivePage = TabSheet1
    Align = alClient
    TabOrder = 0
    object TabSheet1: TTabSheet
      Caption = 'Periods'
      object DBGrid1: TDBGrid
        Left = 0
        Top = 0
        Width = 474
        Height = 417
        Align = alLeft
        DataSource = DataModule1.dsPeriod
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -14
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'ID'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Description'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'BeginWeek'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'EndWeek'
            Visible = True
          end>
      end
    end
    object TabSheet2: TTabSheet
      Caption = 'Weeks'
      ImageIndex = 1
      object Label3: TLabel
        Left = 452
        Top = 273
        Width = 199
        Height = 16
        Caption = 'Use this to lookup week numbers '
      end
      object Label1: TLabel
        Left = 448
        Top = 312
        Width = 242
        Height = 20
        Caption = '(Select Begin date of Week 1)'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -17
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object DBGrid2: TDBGrid
        Left = 8
        Top = 8
        Width = 320
        Height = 401
        DataSource = DataModule1.dsweek
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -14
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
      end
      object DBNavigator1: TDBNavigator
        Left = 336
        Top = 8
        Width = 288
        Height = 49
        DataSource = DataModule1.dsweek
        VisibleButtons = [nbFirst, nbPrior, nbNext, nbLast, nbEdit, nbPost, nbCancel, nbRefresh]
        TabOrder = 1
      end
      object MonthCalendar1: TMonthCalendar
        Left = 432
        Top = 76
        Width = 277
        Height = 189
        MultiSelect = True
        Date = 37076
        EndDate = 37076
        FirstDayOfWeek = dowThursday
        MaxSelectRange = 365
        ParentShowHint = False
        ShowHint = True
        TabOrder = 2
        WeekNumbers = True
      end
      object Button1: TButton
        Left = 452
        Top = 339
        Width = 247
        Height = 70
        Caption = 'Update Week No Start Dates'
        TabOrder = 3
        OnClick = Button1Click
      end
    end
  end
end
