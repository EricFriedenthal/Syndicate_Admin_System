program Syndicate;

uses
  Forms,
  Main in 'Main.pas' {Form1},
  Members in 'Members.pas' {Form2},
  Periods in 'Periods.pas' {Form3},
  data in 'data.pas' {DataModule1: TDataModule},
  Preferences in 'Preferences.pas' {Form4},
  Report in 'Report.pas' {Form5},
  About in 'About.pas' {AboutBox},
  PrefSum in 'PrefSum.pas' {RPrefSum},
  AllocSum in 'AllocSum.pas' {RAllocSum},
  WeekMember in 'WeekMember.pas' {RWeekMember},
  PrefTot in 'PrefTot.pas' {RPrefTot},
  qrMember in 'qrMember.pas' {repMember: TQuickRep};

{$R *.RES}

begin
  Application.Initialize;
  Application.CreateForm(TForm1, Form1);
  Application.CreateForm(TForm2, Form2);
  Application.CreateForm(TForm3, Form3);
  Application.CreateForm(TDataModule1, DataModule1);
  Application.CreateForm(TForm4, Form4);
  Application.CreateForm(TAboutBox, AboutBox);
  Application.CreateForm(TrepMember, repMember);
  Application.Run;
end.
